package com.app.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebFilter(filterName = "AuthFilter", urlPatterns = { "/private/*" })
public class AuthorizationFilter implements Filter {

	public AuthorizationFilter() {
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws IOException, ServletException {
		try {
			
			HttpServletRequest reqt = (HttpServletRequest) request;
			HttpServletResponse resp = (HttpServletResponse) response;
			
			boolean isAjax = "XMLHttpRequest".equals(reqt.getHeader("X-Requested-With"));
			
			if(!isAjax){
				//System.out.println("--------------pas ajax");
				if (!reqt.getRequestURI().endsWith("index.xhtml") && reqt.getSession().getAttribute("AUTHENTICATED") == null) {
					resp.sendRedirect(reqt.getContextPath() + "/index.xhtml");
				}else{
		        	chain.doFilter(request, response);
				}
			}else{
				//System.out.println("--------------ajax");
				if (!reqt.getRequestURI().endsWith("index.xhtml") && reqt.getSession().getAttribute("AUTHENTICATED") == null) {
		            // Redirecting an ajax request has to be done in the following way:
		            // http://javaevangelist.blogspot.com/2013/01/jsf-2x-tip-of-day-ajax-redirection-from.html
		            String redirectURL = resp.encodeRedirectURL(reqt.getContextPath() + "/index.xhtml");
		            StringBuilder sb = new StringBuilder();
		            sb.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?><partial-response><redirect url=\"").append(redirectURL).append("\"></redirect></partial-response>");
		            resp.setCharacterEncoding("UTF-8");
		            resp.setContentType("text/xml");
		            PrintWriter pw = resp.getWriter();
		            pw.println(sb.toString());
		            pw.flush();	
				}
			}

	        
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Override
	public void destroy() {

	}
}