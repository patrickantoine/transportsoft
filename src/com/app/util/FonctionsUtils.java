package com.app.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Key;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;


import org.apache.poi.util.IOUtils;
//import org.primefaces.context.RequestContext;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

import java.util.*;
import javax.activation.*;

public class FonctionsUtils {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String PREFIX = "stream2file";
	public static final String SUFFIX = ".tmp";


	public static void info(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "F�licitation!", message));
    }
     
    public static void warn(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Attention!", message));
    }
     
    public static void error(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Une erreur est survenue!", message));
    }
     
    public static void fatal(String message) {
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Action interrompue!", message));
    }
    
    public static void popAlert(String titre, String lemessage){
        FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, titre, lemessage);
        //RequestContext.getCurrentInstance().showMessageInDialog(message);
    }
    
	public static void popup(String message){
		 //executeClientJavascript("swal('Traitement r�ussi', '"+message.replace("'", " ")+"', 'success').then((value) => {location.reload();})");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "F�licitation!", message));

	}
	
	public static void popupErreur(String message){
		//FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("swal('Impossible de continuer', '"+message.replace("'", " ")+"', 'error').then((value) => {location.reload();})");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Une erreur est survenue!", message));
	}
	
	public static void popupWarning(String message){
		//FacesContext.getCurrentInstance().getPartialViewContext().getRenderIds().add("swal('Attention', '"+message.replace("'", " ")+"', 'warning').then((value) => {location.reload();})");
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Attention!", message));

	}
	
	
	public static void executeClientJavascript(String script) {
		 FacesContext facesContext = FacesContext.getCurrentInstance();
		 ExtendedRenderKitService service = Service.getRenderKitService(facesContext, ExtendedRenderKitService.class);
		 service
		 	.addScript(
		 			facesContext
				 , script);
	}
	
	/**permet de r�cup�rer l'ann�e d'une date donn�e.
	 @param dateRef date debut
	 * @return l'ann�e correspondante � la date.
	 */
	public static int getCurrentYear(Date dateRef){
		   Calendar calendar=createCalendarFromDate(dateRef);
		   return calendar.get(Calendar.YEAR);
   }
	
	/**permet de r�cup�rer le mois d'une date donn�e.
	@param dateRef date debut
	 * @return le mois correspondant � la date.
	 */
	public static int getCurrentMonth(Date dateRef, boolean beginZeroAsJava){
		   Calendar calendar=createCalendarFromDate(dateRef);
		   return beginZeroAsJava ? calendar.get(Calendar.MONTH) : calendar.get(Calendar.MONTH) + 1;
	}
	
	/**permet de r�cup�rer le jour d'une date donn�e.
	 * @param dateRef date debut
	 * @return le jour correspondant � la date
	 */
	public static int getCurrentDay(Date dateRef){
		   Calendar calendar=createCalendarFromDate(dateRef);		   
		   return calendar.get(Calendar.DAY_OF_MONTH);
	}
	
	/**permet de r�cup�rer la fin du mois pr�c�dant la date entr�e en param�tre.
	 * @param dateRef date d�part.
	 * @return la date de fin du mois pr�c�dant.
	 */
	public static Date getDateFinMoisPrecedant(Date dateRef){
		   Date dateFinMoisPrecedant=addNdays(dateRef,-getCurrentDay(dateRef));
		   return dateFinMoisPrecedant;
	}
	/**permet de r�cup�rer la fin du mois suivant la date entr�e en param�tre.
	 * @param dateRef date d�part.
	 * @return la date de fin du mois pr�c�dant.
	 */
	public static Date getDateFinMoisSuivant(Date dateRef){
		   return addNmois(finDuMois(dateRef),1);		   
	}
	/**debut du mois donn� en param�tre.
	@param dateRef date d�part.
	 * @return le debut du mois.
	 */
	public static Date debutDuMois(Date dateRef){
		   return addNdays(getDateFinMoisPrecedant(dateRef),1);
	}
	/**le mois donn� en param�tre.
	@param dateRef date en question.
	 * @return la fin du mois.
	 */
	public static Date finDuMois(Date dateRef){
	     Date dsLeMoisSuivant=addNdays(getDateFinMoisPrecedant(dateRef),32);
	     return addNdays(dsLeMoisSuivant, -getCurrentDay(dsLeMoisSuivant));
   }
	
	/**permet de r�cup�rer le debut du mois suivant la date entr�e en param�tre.
	 * @param dateRef date d�part.
	 * @return la date de fin du mois pr�c�dant.
	 */
	public static Date getDateDebutMoisSuivant(Date dateRef){
		 if(dateRef == null) return null;
	     Date dsLeMoisSuivant=addNdays(getDateFinMoisPrecedant(dateRef),33);
	     return debutDuMois(dsLeMoisSuivant);
   }
	/**permet de r�cup�rer la ni�me fin de mois pr�c�dant la date entr�e en param�tre.
	* @param dateRef date depart.
	 * @param n nombre de jours � soustraire.
	 * @return la date finale.*/
	public static Date getNiemeDateFinMoisPrecedant(Date dateRef,int n){
		   Date result = dateRef;
		   for(int i=n ;i>0; i--){			 
			   result = getDateFinMoisPrecedant(result);
		   }
		   return result;
	}
	
	/**permet de formatter la date entr�e en param�tre.
	 * @param format format 
	 * @param locale pour la langue
	 * @param date date � formatter
	 * @return date formatt�e*/
	public static String formateDate(Date date,String format, Locale locale){			
		return new SimpleDateFormat(format,locale).format(date);
	}

	/**permet de formatter la date entr�e en param�tre.
	 * @param format format 
	 * @param date date � formatter
	 * @return date formatt�e*/
	public static String formateDate(Date date,String format){
		if(date == null)return "";
		return new SimpleDateFormat(format).format(date);
	}

	
	/**ajoute n jours � une date donn�e.
	* @param datDeb date depart.
	 * @param n nombre de jours � ajouter.
	 * @return la date finale.*/
	@SuppressWarnings("static-access")
	public static Date addNdays(Date datDeb,int n){
			Calendar calendar = Calendar.getInstance();
		    calendar.setTime(datDeb);
		    Calendar calendar2 = Calendar.getInstance();
		    calendar2.clear();
		    calendar2.set(calendar.get(calendar.YEAR), calendar.get(calendar.MONTH),calendar.get(calendar.DAY_OF_MONTH)+n);
		    return calendar2.getTime();
	}
	
	@SuppressWarnings("static-access")
	public static Date moinsNdays(Date datDeb,int n){
			Calendar calendar = Calendar.getInstance();
		    calendar.setTime(datDeb);
		    Calendar calendar2 = Calendar.getInstance();
		    calendar2.clear();
		    calendar2.set(calendar.get(calendar.YEAR), calendar.get(calendar.MONTH),calendar.get(calendar.DAY_OF_MONTH)-n);
		    return calendar2.getTime();
	}
	
	@SuppressWarnings("static-access")
	public static Date addNminutes(Date datDeb,int n){
			Calendar calendar = Calendar.getInstance();
		    calendar.setTime(datDeb);		    
		    calendar.set(Calendar.MINUTE, calendar.get(calendar.MINUTE)+n);
		    return calendar.getTime();
	}

	/**ajoute n jours � une date donn�e.
	* @param datDeb date depart.
	 * @param n nombre de jours � ajouter.
	 * @return la date finale.*/
	@SuppressWarnings("static-access")
	public static Date addAnnees(Date datDeb,int n){
			Calendar calendar = Calendar.getInstance();
		    calendar.setTime(datDeb);
		    Calendar calendar2 = Calendar.getInstance();
		    calendar2.clear();
		    calendar2.set(calendar.get(calendar.YEAR)+n, calendar.get(calendar.MONTH),calendar.get(calendar.DAY_OF_MONTH));
		    return calendar2.getTime();
	}
	
	/** permet de constituer une date � partir de l'ann�e, le mois et le jour.
	 * @param annee l'ann�e.
	 * @param mois le mois.
	 * @param jour jour.
	 * @param heure 
	 * @param mn
	 * @param second  
	 * @return la date constitu�e.*/
	 
	public static Date fixerTime(Date date,int heure, int mn,int second){
		    Calendar calendar = Calendar.getInstance();	
		    calendar.clear();
		    calendar.setTime(date);
		    calendar.set(Calendar.HOUR_OF_DAY, heure);
		    calendar.set(Calendar.MINUTE, mn);
		    calendar.set(Calendar.SECOND, second);
		    return calendar.getTime();
	}

	/** permet de constituer une date � partir de l'ann�e, le mois et le jour.
	 * @param annee l'ann�e.
	 * @param mois le mois.
	 * @param jour jour.
	 * @return la date constitu�e.*/
	public static Date constituerDate(int annee , int mois, int jour){
		    Calendar calendar = Calendar.getInstance();	
		    calendar.clear();
		    calendar.set(annee, mois, jour);
		    return calendar.getTime();
	}	
	
	/**m�thode d'initialisation d'un objet calendar.
	 * permettant de manipuler les dates
	 * @param date date en question.
	 * @return un objet calendar.**/
	public static Calendar createCalendarFromDate(Date date){
		java.util.Calendar calendar=Calendar.getInstance();		
		calendar.clear();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);	
		return calendar;
    }	

	public static Date addNmois(Date datDeb,int n){
		  if(datDeb == null) return null;
		  Calendar deb = createCalendarFromDate(datDeb);
		  boolean ok = ecartEnjours(datDeb, finDuMois(datDeb))==0;
		  deb.add(Calendar.DAY_OF_MONTH, ok ? 1 : 0);//permet de se positionner sur les fins du mois 
		  deb.add(Calendar.MONTH,n);
		  deb.add(Calendar.DAY_OF_MONTH, ok ? -1 : 0);//
		  return deb.getTime();
	}
	
	/**ecart en jours entre deux dates.
	 * @param deb debut
	 * @param fin fin
	 * @return le nombre de jours.**/	
	public static int ecartEnjours(Date deb,Date fin){
		  //if(deb.after(fin))return 0;	//tr�s dangereux			
		  deb = fixerTime(deb, 0, 0, 0);		  
		  fin = fixerTime(fin, 0, 0, 0);
		  Long nmillis=fin.getTime()-deb.getTime();
		  return Integer.parseInt(Math.round(nmillis*1d/(1000*60*60*24))+"");
	}
	
    public static boolean date1AfterDate2(Date date1, Date date2){
    	Calendar cal1 = createCalendarFromDate(date1);
    	Calendar cal2 = createCalendarFromDate(date2);    	
    	//System.out.println(cal1.getTime());
    	//System.out.println(cal2.getTime());
    	return cal1.after(cal2);
    }
    
	public static String GenererNumero(Long id,String prefix){
		int curYear = Calendar.getInstance().get(Calendar.YEAR);
		return prefix+curYear+String.format("%07d", id);
	}
	
    public static void copyFile(String fileName, InputStream in) throws IOException {
	             OutputStream out = new FileOutputStream(new File("C:\\rep\\" + fileName));
	           
	             int read = 0;
	             byte[] bytes = new byte[1024];
	           
	             while ((read = in.read(bytes)) != -1) {
	                 out.write(bytes, 0, read);
	             }
	           
	             in.close();
	             out.flush();
	             out.close();
	           

    }
    
    public static String getMoisEnLettre(int mois){
    	String [] moisTab = {"","Janvier", "F�vrier", "Mars" , "Avril","Mai","Juin","Juillet","Aout","Septembre","Octobre","Novembre","D�cembre"};
    	return moisTab[mois];
    }
    
    public static String getRandomNumero() {
        int i = (int) (Math.random() * 10000000);
         
        return String.valueOf(i);
    }
    
	public static String encrypt(String password,String key){
		try{
			Key clef = new SecretKeySpec(key.getBytes("ISO-8859-2"),"Blowfish");
			Cipher cipher=Cipher.getInstance("Blowfish");
			cipher.init(Cipher.ENCRYPT_MODE,clef);
			return new String(cipher.doFinal(password.getBytes()));
		}catch (Exception e){
			return null;
		}
	}
	
    public static File stream2file (InputStream in) throws IOException {
        final File tempFile = File.createTempFile(PREFIX, SUFFIX);
        tempFile.deleteOnExit();
        try (FileOutputStream out = new FileOutputStream(tempFile)) {
            IOUtils.copy(in, out);
        }
        return tempFile;
    }
    
    
    
/*    @SuppressWarnings("unused")
	public static void envoyerMail( String destinataire, String sujet, String message, InputStream piecesJointe,String typeDoc) throws MessagingException, IOException
    {
        //Configurer serveur smtp
        Properties props=new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.ssl.trust", "smtp.gmail.com");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth", "true");
        
        //Envoyer pseudo et mot de passe
        Session session=Session.getInstance(props, new Authenticator(){
            @Override
            protected PasswordAuthentication getPasswordAuthentication(){
                return new PasswordAuthentication("antoinendong85@gmail.com", "webmaster2012");
            }
        });
        
        //Message et pi�ce jointe
        //Cr�er message
        Message msg=new MimeMessage(session);
        //Multipart
        Multipart multipart=new MimeMultipart();
        
        //Corps (texte)
        BodyPart partieMessage=new MimeBodyPart();
        //partieMessage.setText(message); //Pour envoyer texte brute
        partieMessage.setContent(message, "text/html"); //Pour envoyer HTML
        multipart.addBodyPart(partieMessage);

        //Pi�ce jointe
        partieMessage=new MimeBodyPart();
        DataSource source = new FileDataSource(stream2file(piecesJointe));
        partieMessage.setDataHandler(new DataHandler(source));
        partieMessage.setFileName(typeDoc+".pdf");
        multipart.addBodyPart(partieMessage);
        
        //Configurer exp�diteur
        InternetAddress adrExpediteur=new InternetAddress("antoinendong85@yahoo.fr");
        msg.setFrom(adrExpediteur);
        
        //DESTINATAIRES
        //Configurer destinataires
        InternetAddress[] adrDestinataires=new InternetAddress[1];
        adrDestinataires[0]=new InternetAddress(destinataire);
        msg.setRecipients(Message.RecipientType.TO, adrDestinataires);
        
        //Sujet et corps du message
        msg.setSubject(sujet);
        msg.setContent(multipart);
        Transport.send(msg);
    }
	*/
	
}
