package com.app.util;

import org.hibernate.*;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.*;

import com.transport.entity.Adresse;
import com.transport.entity.Licence;
import com.transport.entity.Personne;
import com.transport.entity.Profil;
import com.transport.entity.ProfilRole;
import com.transport.entity.Province;
import com.transport.entity.Role;
import com.transport.entity.Societe;
import com.transport.entity.SocieteActivite;
import com.transport.entity.Type;
import com.transport.entity.UserProfil;
import com.transport.entity.Utilisateur;
import com.transport.entity.Ville;
import com.transport.entity.Voiture;



public class HibernateUtil {
	private static SessionFactory sessionFactory;
	
    static {
    	
    	try {
    		Configuration configuration = new Configuration().configure();
			
			configuration.addAnnotatedClass(Utilisateur.class);
			configuration.addAnnotatedClass(Personne.class);
			configuration.addAnnotatedClass(Adresse.class);
			configuration.addAnnotatedClass(Province.class);
			configuration.addAnnotatedClass(Societe.class);
			configuration.addAnnotatedClass(SocieteActivite.class);
			configuration.addAnnotatedClass(Type.class);
			configuration.addAnnotatedClass(Ville.class);
			configuration.addAnnotatedClass(Voiture.class);
			configuration.addAnnotatedClass(Licence.class);
			configuration.addAnnotatedClass(Role.class);
			configuration.addAnnotatedClass(Profil.class);
			configuration.addAnnotatedClass(UserProfil.class);
			configuration.addAnnotatedClass(ProfilRole.class);

			
    		
    		StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
    		sessionFactory = configuration.buildSessionFactory(builder.build());
        } catch (Throwable ex) {
            // Gestion exception
            System.err.println("Echec cr�ation SessionFactory" + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
    
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    // Call this during shutdown
    public static void close() {
    	sessionFactory.close();
    }


}
