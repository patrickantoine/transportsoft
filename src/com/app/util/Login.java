package com.app.util;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.transport.entity.Utilisateur;
import com.transport.services.ProjetService;







@Named("login")
@SessionScoped
public class Login implements Serializable {

	private static final long serialVersionUID = 1094801825228386363L;
	
	@Inject
	private ProjetService projetService;
	
	
	
	private String pwd;
	private String msg;
	private String user;
	private Utilisateur userConnecte;
	private List<Long> habilitation = new ArrayList<Long>();

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	//validate login
	@SuppressWarnings("unchecked")
	public String validateUsernamePassword() {
		userConnecte = LoginDAO.validate(user, pwd);
		String redirect = "";
		boolean ok = true;
		String msg = "";
		if (userConnecte != null) {
			/*if(!userConnecte.getActif()){
				ok =false;
				msg = "Votre compte est d�sactiv�...";
			}
			
			if(utilisateurService.isConnect(userConnecte)){
				ok =false;
				msg = "Vous avez d�j� une session ouverte sur un autre ordinateur...";
			}*/			
			
			userConnecte.setDateDerniereConn(new Date());
	    	Session dataSource = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = dataSource.beginTransaction();
	    	trans.begin();
	    	
	    	habilitation = dataSource.createQuery("select pr.role.id from ProfilRole pr where pr.profil = :param").setParameter("param", userConnecte.getProfil()).list();
	    	
	    	List<Long> menuHabilitation = dataSource.createQuery("select pr.role.roleParent.id from ProfilRole pr where pr.profil = :param").setParameter("param", userConnecte.getProfil()).list();
	    	
	    	habilitation.addAll(menuHabilitation);
	    	
	    	dataSource.update(userConnecte);
	    	trans.commit();
			
			if(ok){
				projetService.chargerInfo();
				
				HttpSession session = (HttpSession) SessionUtils.getSession();
				session.setAttribute("username", user);
				//session.setAttribute("userObjet", userConnecte);
				session.setAttribute("AUTHENTICATED", true);
				//referentielService.getListUserConnect().add(userConnecte);
				
				redirect = "/private/home.xhtml?faces-redirect=true";
				
				return redirect;
			}else{
				FonctionsUtils.popupErreur(msg);
				return "";
			}

		} else {

			FonctionsUtils.popupErreur("Login ou mot de passe incorrect...");
			
			return "/index.xhtml";
		}
	}

	//logout event, invalidate session
	public String logout() {
		HttpSession session = (HttpSession) SessionUtils.getSession();
		session.invalidate();
		return "/index.xhtml?faces-redirect=true";
	}

	public Utilisateur getUserConnecte() {
		return userConnecte;
	}

	public void setUserConnecte(Utilisateur userConnecte) {
		this.userConnecte = userConnecte;
	}

	/**
	 * @return the habilitation
	 */
	public List<Long> getHabilitation() {
		return habilitation;
	}

	/**
	 * @param habilitation the habilitation to set
	 */
	public void setHabilitation(List<Long> habilitation) {
		this.habilitation = habilitation;
	}
}
