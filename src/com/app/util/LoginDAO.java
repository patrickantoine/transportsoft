package com.app.util;


import java.security.Key;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import org.hibernate.Session;
import org.hibernate.Transaction;
import com.transport.entity.Utilisateur;




public class LoginDAO {
	

	
	@SuppressWarnings("unchecked")
	public static Utilisateur validate(String user, String password) {

		Utilisateur userConnecte = null;

    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	userConnecte = (Utilisateur) session.createQuery("from Utilisateur u "
    			+ "where u.login = :paramUser "
    			+ "and u.mdp = :paramPass"
    			+ "")
    			.setParameter("paramUser", user)
    			.setParameter("paramPass", encrypt(password,user))
    			.uniqueResult();
    	
		return userConnecte;
		
	}
	
	public static String encrypt(String password,String key){
		try{
			Key clef = new SecretKeySpec(key.getBytes("ISO-8859-2"),"Blowfish");
			Cipher cipher=Cipher.getInstance("Blowfish");
			cipher.init(Cipher.ENCRYPT_MODE,clef);
			return new String(cipher.doFinal(password.getBytes()));
		}catch (Exception e){
			return null;
		}
	}
	
}