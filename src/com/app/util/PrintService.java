package com.app.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.ServletContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

@SessionScoped
@Named("printService")
public class PrintService implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static StreamedContent streamedContent;
	
	private static InputStream inputStream = null;
	
	

	public static void genererEtat(List<Long>listID,String fichierJRXML,String nomFichierSortie,Map<String,Object> autreParams,String nomFichier) throws JRException, IOException{
	    // - Param�tres de connexion � la base de donn�es
	    String url = "jdbc:mysql://localhost:3306/transport_bd";
	    String login = "root";
	    String password = "azerty";
	    Connection connection = null;

            try {
  	          // - Connexion � la base
                Driver monDriver = new org.postgresql.Driver();
				DriverManager.registerDriver(monDriver);
				connection = DriverManager.getConnection(url, login, password);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
            // - Chargement et compilation du rapport
   			ExternalContext ec =	FacesContext.getCurrentInstance().getExternalContext();
   			ServletContext sc = (ServletContext)ec.getContext();
   			
   			String reportFile = sc.getRealPath("/private/etats/");
   			
            //JasperDesign jasperDesign = JRXmlLoader.load("C:\\webapp_file\\ireport\\"+fichierJRXML+".jrxml");
   		    JasperDesign jasperDesign = JRXmlLoader.load(reportFile+"/"+fichierJRXML+".jrxml");
            JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);            

           // - Param�tres � envoyer au rapport
           Map parameters = new HashMap();
           parameters.put("LISTDONNEE", listID);
           
           if(autreParams!= null && !autreParams.isEmpty()){
        	   for(Entry<String, Object> entry : autreParams.entrySet()) {
        		    String cle = entry.getKey();
        		    Object valeur = entry.getValue();

        		    parameters.put(cle, valeur);
        		}
           }

           // - Execution du rapport
           JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,connection);

           // - Cr�ation du rapport au format PDF
           byte[] docPdf = JasperExportManager.exportReportToPdf(jasperPrint);
           
           
           
           InputStream is = new ByteArrayInputStream(docPdf);
           inputStream = is;
           streamedContent = new DefaultStreamedContent(is, "application/pdf",nomFichier);
           
           
           
           //OutputStream output = new FileOutputStream(new File("C:\\webapp_file\\print_temp\\"+nomFichier+".pdf"));
           //JasperExportManager.exportReportToPdfStream(jasperPrint, output);
           //output.close();
           try {
        	   is.close();
               connection.close();
              } catch (SQLException e) {
               e.printStackTrace();
           }
	}

	public StreamedContent getStreamedContent() {
		return streamedContent;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	
}

