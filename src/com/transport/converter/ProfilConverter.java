package com.transport.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.transport.entity.Profil;
import com.transport.services.ReferentielService;

@FacesConverter("profilConverter")
public class ProfilConverter implements Converter {

	  @Override
	    public Object getAsObject(FacesContext ctx, UIComponent uiComponent, String Id) {
	        ValueExpression vex =
	                ctx.getApplication().getExpressionFactory()
	                        .createValueExpression(ctx.getELContext(),
	                                "#{referentielService}", ReferentielService.class);

	        ReferentielService referentielService = (ReferentielService)vex.getValue(ctx.getELContext());
	        
	        try{
	        	return referentielService.returnProfil(new Long(Id));
	        }catch(Exception e){
	        	return null;
	        }
	    }

	    @Override
	    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object objet) {
	        
	    	try{
	    		return ((Profil)objet).getId().toString();
	    	}catch(Exception e){
	    		return null;
	    	}
	    }
}
