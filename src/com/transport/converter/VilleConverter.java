package com.transport.converter;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import com.transport.entity.Province;
import com.transport.entity.Type;
import com.transport.entity.Ville;
import com.transport.services.ReferentielService;

@FacesConverter("villeConverter")
public class VilleConverter implements Converter {

	  @Override
	    public Object getAsObject(FacesContext ctx, UIComponent uiComponent, String Id) {
	        ValueExpression vex =
	                ctx.getApplication().getExpressionFactory()
	                        .createValueExpression(ctx.getELContext(),
	                                "#{referentielService}", ReferentielService.class);

	        ReferentielService referentielService = (ReferentielService)vex.getValue(ctx.getELContext());
	        
	        try{
	        	return referentielService.returnVille(new Long(Id));
	        }catch(Exception e){
	        	return null;
	        }
	    }

	    @Override
	    public String getAsString(FacesContext facesContext, UIComponent uiComponent, Object objet) {
	        
	    	try{
	    		return ((Ville)objet).getId().toString();
	    	}catch(Exception e){
	    		return null;
	    	}
	    }
}
