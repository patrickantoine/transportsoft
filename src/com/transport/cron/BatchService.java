package com.transport.cron;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;

@Singleton
@Startup
public class BatchService {
	
    @Inject
    private CdiJobFactory jobFactory;
    
    private Scheduler scheduler1;
    
    
    @PostConstruct
	public void batch(){
		try {
	
		    JobDetail job1 = JobBuilder.newJob(DesactiveLicence.class)
		            .withIdentity("job1", "group1").build();
		
		    Trigger trigger1 = TriggerBuilder.newTrigger()
		            .withIdentity("cronTrigger1", "group1")
		            .withSchedule(CronScheduleBuilder.cronSchedule("0 0 1 ? * * *"))
		            .build();
		     
		    scheduler1 = new StdSchedulerFactory().getScheduler();
		    scheduler1.setJobFactory(jobFactory);
		    scheduler1.start();
		    scheduler1.scheduleJob(job1, trigger1);
		    
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	}
    
    @PreDestroy
    public void preDestroy() throws SchedulerException {
        if (scheduler1 != null && scheduler1.isStarted()) {
        	scheduler1.shutdown(false);
        }
       
        
		
    }
    

}