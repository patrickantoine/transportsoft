package com.transport.cron;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.app.util.HibernateUtil;


@ApplicationScoped
public class DesactiveLicence implements Job {
	

	
	@SuppressWarnings("unchecked")
	public void execute(JobExecutionContext context) throws JobExecutionException {
		
		
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	int nb = session.createQuery("update Licence lic set lic.active = false where lic.dateFin < NOW() and lic.active = true").executeUpdate();
	    	
		trans.commit();
		
		//System.out.println("-----"+nb+" licences désactivées.");
		
	}

}
