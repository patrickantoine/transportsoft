package com.transport.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Adresse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String numeroVilla;
	private String rue;
	private String bp;
	private String lieuDit;
	private Ville ville;
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNumeroVilla() {
		return numeroVilla;
	}
	public void setNumeroVilla(String numeroVilla) {
		this.numeroVilla = numeroVilla;
	}
	public String getRue() {
		return rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}
	public String getBp() {
		return bp;
	}
	public void setBp(String bp) {
		this.bp = bp;
	}
	public String getLieuDit() {
		return lieuDit;
	}
	public void setLieuDit(String lieuDit) {
		this.lieuDit = lieuDit;
	}
	@OneToOne
	@JoinColumn(name = "idville", nullable=true)
	public Ville getVille() {
		return ville;
	}
	public void setVille(Ville ville) {
		this.ville = ville;
	}
	

}
