package com.transport.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.app.util.FonctionsUtils;

@Entity
public class Licence implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Type typeLicence;
	private Date dateCreation;
	private Date dateDebut;
	private Date dateFin;
	private Voiture voiture;
	private Type typeActivite;
	private Boolean active = true;
	private Boolean renouvelle = false; // s'il cette licence a d�j� �t� renouvell�e
	private Utilisateur userCreation;
	private Boolean renouvellement = false;// s'il s'agit d'un renouvellement ou d'une nouvelle licence
	private Integer duree = 0;
	private String numeroLicence;
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@OneToOne
	@JoinColumn(name = "idtypelicence", nullable=false)
	public Type getTypeLicence() {
		return typeLicence;
	}
	public void setTypeLicence(Type typeLicence) {
		this.typeLicence = typeLicence;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	public Date getDateDebut() {
		return dateDebut;
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	@OneToOne
	@JoinColumn(name = "idvoiture", nullable=false)
	public Voiture getVoiture() {
		return voiture;
	}
	public void setVoiture(Voiture voiture) {
		this.voiture = voiture;
	}
	@OneToOne
	@JoinColumn(name = "idtypeactivite", nullable=false)
	public Type getTypeActivite() {
		return typeActivite;
	}
	public void setTypeActivite(Type typeActivite) {
		this.typeActivite = typeActivite;
	}
	/**
	 * @return the active
	 */
	public Boolean getActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}
	/**
	 * @return the userCreation
	 */
	@OneToOne
	@JoinColumn(name = "idusercreation", nullable=true)
	public Utilisateur getUserCreation() {
		return userCreation;
	}
	/**
	 * @param userCreation the userCreation to set
	 */
	public void setUserCreation(Utilisateur userCreation) {
		this.userCreation = userCreation;
	}
	/**
	 * @return the renouvellement
	 */
	public Boolean getRenouvellement() {
		return renouvellement;
	}
	/**
	 * @param renouvellement the renouvellement to set
	 */
	public void setRenouvellement(Boolean renouvellement) {
		this.renouvellement = renouvellement;
	}
	/**
	 * @return the duree
	 */
	@Transient
	public Integer getDuree() {
		if(FonctionsUtils.date1AfterDate2(new Date(), this.dateFin)) {
			return 0;
		}else {
			return FonctionsUtils.ecartEnjours(new Date(), this.dateFin);
		}
		
	}
	/**
	 * @param duree the duree to set
	 */
	public void setDuree(Integer duree) {
		this.duree = duree;
	}
	public Boolean getRenouvelle() {
		return renouvelle;
	}
	public void setRenouvelle(Boolean renouvelle) {
		this.renouvelle = renouvelle;
	}
	public String getNumeroLicence() {
		return numeroLicence;
	}
	public void setNumeroLicence(String numeroLicence) {
		this.numeroLicence = numeroLicence;
	}
	

}
