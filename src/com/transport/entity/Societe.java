package com.transport.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
public class Societe implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String libelle;
	private String code;
	private Personne gerant;
	private Adresse adresse;
	private Date dateCreation;
	private Date dateDebutActivite;
	private List<Voiture> listVoiture;
	private Type formeJuridique;
	private Boolean active = true;
	private Boolean affecter = false;
	private Date dateAffectation;
	private Utilisateur userCreation;
	private String commentaire;
	private Utilisateur userModif;
	private Date dateModif;
	private Utilisateur gestionnaire;
	
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	/**
	 * @return the gerant
	 */
	@OneToOne
	@JoinColumn(name = "idgerant", nullable=false)
	public Personne getGerant() {
		return gerant;
	}
	/**
	 * @param gerant the gerant to set
	 */
	public void setGerant(Personne gerant) {
		this.gerant = gerant;
	}
	/**
	 * @return the adresse
	 */
	@OneToOne
	@JoinColumn(name = "idadresse", nullable=false)
	public Adresse getAdresse() {
		return adresse;
	}
	/**
	 * @param adresse the adresse to set
	 */
	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}
	
	/**
	 * @return the dateCreation
	 */
	public Date getDateCreation() {
		return dateCreation;
	}
	/**
	 * @param dateCreation the dateCreation to set
	 */
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	/**
	 * @return the dateDebutActivite
	 */
	public Date getDateDebutActivite() {
		return dateDebutActivite;
	}
	/**
	 * @param dateDebutActivite the dateDebutActivite to set
	 */
	public void setDateDebutActivite(Date dateDebutActivite) {
		this.dateDebutActivite = dateDebutActivite;
	}

	/**
	 * @return the formeJuridique
	 */
	@OneToOne
	@JoinColumn(name = "idformejuridique", nullable=true)
	public Type getFormeJuridique() {
		return formeJuridique;
	}
	/**
	 * @param formeJuridique the formeJuridique to set
	 */
	public void setFormeJuridique(Type formeJuridique) {
		this.formeJuridique = formeJuridique;
	}
	/**
	 * @return the listVoiture
	 */
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name="idtransporteur",nullable=true)
	@Cascade(value=(CascadeType.ALL))
	public List<Voiture> getListVoiture() {
		return listVoiture;
	}
	/**
	 * @param listVoiture the listVoiture to set
	 */
	public void setListVoiture(List<Voiture> listVoiture) {
		this.listVoiture = listVoiture;
	}
	/**
	 * @return the active
	 */
	public Boolean getActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	
	public Boolean getAffecter() {
		return affecter;
	}
	public void setAffecter(Boolean affecter) {
		this.affecter = affecter;
	}
	/**
	 * @return the userCreation
	 */
	@OneToOne
	@JoinColumn(name = "idusercreation", nullable=true)
	public Utilisateur getUserCreation() {
		return userCreation;
	}
	/**
	 * @param userCreation the userCreation to set
	 */
	public void setUserCreation(Utilisateur userCreation) {
		this.userCreation = userCreation;
	}
	/**
	 * @return the commentaire
	 */
	public String getCommentaire() {
		return commentaire;
	}
	/**
	 * @param commentaire the commentaire to set
	 */
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	/**
	 * @return the userModif
	 */
	@OneToOne
	@JoinColumn(name = "idusermodif", nullable=true)
	public Utilisateur getUserModif() {
		return userModif;
	}
	/**
	 * @param userModif the userModif to set
	 */
	public void setUserModif(Utilisateur userModif) {
		this.userModif = userModif;
	}
	/**
	 * @return the dateModif
	 */
	public Date getDateModif() {
		return dateModif;
	}
	/**
	 * @param dateModif the dateModif to set
	 */
	public void setDateModif(Date dateModif) {
		this.dateModif = dateModif;
	}
	@OneToOne
	@JoinColumn(name = "iduserGestionnaire", nullable=true)
	public Utilisateur getGestionnaire() {
		return gestionnaire;
	}
	public void setGestionnaire(Utilisateur gestionnaire) {
		this.gestionnaire = gestionnaire;
	}
	public Date getDateAffectation() {
		return dateAffectation;
	}
	public void setDateAffectation(Date dateAffectation) {
		this.dateAffectation = dateAffectation;
	}
	
	

}
