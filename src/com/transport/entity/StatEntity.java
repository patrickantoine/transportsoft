package com.transport.entity;

import java.io.Serializable;
import java.util.Date;

public class StatEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String objet;
	private Double montant=0d;
	private Date date;
	private String commentaire;
	
	
	
	public StatEntity(String objet, Double montant, Date date, String commentaire) {
		super();
		this.objet = objet;
		this.montant = montant;
		this.date = date;
		this.commentaire = commentaire;
	}
	
	public String getObjet() {
		return objet;
	}
	public void setObjet(String objet) {
		this.objet = objet;
	}
	public Double getMontant() {
		return montant;
	}
	public void setMontant(Double montant) {
		this.montant = montant;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

}
