package com.transport.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
public class Type implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String libelle;
	private String code;
	private Double montant = 0d;
	private Boolean contextActivite = false;
	private Boolean contextSociete = false;
	private Boolean contextLicence = false;
	private Boolean contextVoiture = false;
	private Boolean contextMotorisation = false;
	private Boolean contextMarque = false;
	private Boolean contextModele = false;
	private Boolean contextJuridique = false;
	private Type typeParent;
	private Boolean coche = false;
	private List<Type> listTypeEnfant;
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Boolean getContextActivite() {
		return contextActivite;
	}
	public void setContextActivite(Boolean contextActivite) {
		this.contextActivite = contextActivite;
	}
	public Boolean getContextSociete() {
		return contextSociete;
	}
	public void setContextSociete(Boolean contextSociete) {
		this.contextSociete = contextSociete;
	}
	public Boolean getContextLicence() {
		return contextLicence;
	}
	public void setContextLicence(Boolean contextLicence) {
		this.contextLicence = contextLicence;
	}
	public Boolean getContextVoiture() {
		return contextVoiture;
	}
	public void setContextVoiture(Boolean contextVoiture) {
		this.contextVoiture = contextVoiture;
	}
	/**
	 * @return the montant
	 */
	public Double getMontant() {
		return montant;
	}
	/**
	 * @param montant the montant to set
	 */
	public void setMontant(Double montant) {
		this.montant = montant;
	}
	/**
	 * @return the contextMotorisation
	 */
	public Boolean getContextMotorisation() {
		return contextMotorisation;
	}
	/**
	 * @param contextMotorisation the contextMotorisation to set
	 */
	public void setContextMotorisation(Boolean contextMotorisation) {
		this.contextMotorisation = contextMotorisation;
	}
	/**
	 * @return the contextMarque
	 */
	public Boolean getContextMarque() {
		return contextMarque;
	}
	/**
	 * @param contextMarque the contextMarque to set
	 */
	public void setContextMarque(Boolean contextMarque) {
		this.contextMarque = contextMarque;
	}
	/**
	 * @return the contextModele
	 */
	public Boolean getContextModele() {
		return contextModele;
	}
	/**
	 * @param contextModele the contextModele to set
	 */
	public void setContextModele(Boolean contextModele) {
		this.contextModele = contextModele;
	}
	/**
	 * @return the typeParent
	 */
	@OneToOne
	@JoinColumn(name = "idparent", nullable=true)
	public Type getTypeParent() {
		return typeParent;
	}
	/**
	 * @param typeParent the typeParent to set
	 */
	public void setTypeParent(Type typeParent) {
		this.typeParent = typeParent;
	}
	/**
	 * @return the coche
	 */
	@Transient
	public Boolean getCoche() {
		return coche;
	}
	/**
	 * @param coche the coche to set
	 */
	public void setCoche(Boolean coche) {
		this.coche = coche;
	}
	/**
	 * @return the contextJuridique
	 */
	public Boolean getContextJuridique() {
		return contextJuridique;
	}
	/**
	 * @param contextJuridique the contextJuridique to set
	 */
	public void setContextJuridique(Boolean contextJuridique) {
		this.contextJuridique = contextJuridique;
	}
	/**
	 * @return the listTypeEnfant
	 */
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name="idparent",nullable=true)
	@Cascade(value=(CascadeType.ALL))
	public List<Type> getListTypeEnfant() {
		return listTypeEnfant;
	}
	/**
	 * @param listTypeEnfant the listTypeEnfant to set
	 */
	public void setListTypeEnfant(List<Type> listTypeEnfant) {
		this.listTypeEnfant = listTypeEnfant;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Type other = (Type) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		return true;
	}
	
	

}
