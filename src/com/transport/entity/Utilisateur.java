package com.transport.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;


@Entity
public class Utilisateur implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String login;
	private String mdp;
	private Personne personne;
	private String fonction;
	private String mdpTemp;
	private Date dateCreation;
	private Date dateDerniereConn;
	private Profil profil;
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	@OneToOne
	@JoinColumn(name = "idpersonne", nullable=false)
	public Personne getPersonne() {
		return personne;
	}
	public void setPersonne(Personne personne) {
		this.personne = personne;
	}
	public String getFonction() {
		return fonction;
	}
	public void setFonction(String fonction) {
		this.fonction = fonction;
	}
	/**
	 * @return the mdpTemp
	 */
	@Transient
	public String getMdpTemp() {
		return mdpTemp;
	}
	/**
	 * @param mdpTemp the mdpTemp to set
	 */
	public void setMdpTemp(String mdpTemp) {
		this.mdpTemp = mdpTemp;
	}
	/**
	 * @return the dateCreation
	 */
	public Date getDateCreation() {
		return dateCreation;
	}
	/**
	 * @param dateCreation the dateCreation to set
	 */
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	/**
	 * @return the dateDerniereConn
	 */
	public Date getDateDerniereConn() {
		return dateDerniereConn;
	}
	/**
	 * @param dateDerniereConn the dateDerniereConn to set
	 */
	public void setDateDerniereConn(Date dateDerniereConn) {
		this.dateDerniereConn = dateDerniereConn;
	}
	/**
	 * @return the profil
	 */
	@OneToOne
	@JoinColumn(name = "idprofil", nullable=true)
	public Profil getProfil() {
		return profil;
	}
	/**
	 * @param profil the profil to set
	 */
	public void setProfil(Profil profil) {
		this.profil = profil;
	}
	/**
	 * @return the mdpTemp2
	 */


}
