package com.transport.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
public class Voiture implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private Type modele;
	private Type categorie;
	private Type motorisation;
	private Licence licence;
	private String numeroChassi;
	private String numeroImmat;
	private Integer annee = 0;
	private Date dateMiseCirculation;
	private Integer nombrePlace = 0;
	private String commentaire;
	private Date dateCreation;
	private Societe transporteur;
	private Utilisateur userCreation;
	
	
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	@OneToOne
	@JoinColumn(name = "idmodele", nullable=false)
	public Type getModele() {
		return modele;
	}
	public void setModele(Type modele) {
		this.modele = modele;
	}
	@OneToOne
	@JoinColumn(name = "idcategorie", nullable=true)
	public Type getCategorie() {
		return categorie;
	}
	public void setCategorie(Type categorie) {
		this.categorie = categorie;
	}
	@OneToOne
	@JoinColumn(name = "idmotorisation", nullable=false)
	public Type getMotorisation() {
		return motorisation;
	}
	public void setMotorisation(Type motorisation) {
		this.motorisation = motorisation;
	}
	public String getNumeroChassi() {
		return numeroChassi;
	}
	public void setNumeroChassi(String numeroChassi) {
		this.numeroChassi = numeroChassi;
	}
	public String getNumeroImmat() {
		return numeroImmat;
	}
	public void setNumeroImmat(String numeroImmat) {
		this.numeroImmat = numeroImmat;
	}
	public Integer getAnnee() {
		return annee;
	}
	public void setAnnee(Integer annee) {
		this.annee = annee;
	}
	public Date getDateMiseCirculation() {
		return dateMiseCirculation;
	}
	public void setDateMiseCirculation(Date dateMiseCirculation) {
		this.dateMiseCirculation = dateMiseCirculation;
	}
	public Integer getNombrePlace() {
		return nombrePlace;
	}
	public void setNombrePlace(Integer nombrePlace) {
		this.nombrePlace = nombrePlace;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	/**
	 * @return the licence
	 */
	@OneToOne
	@JoinColumn(name = "idlicenceencours", nullable=true)
	public Licence getLicence() {
		return licence;
	}
	/**
	 * @param licence the licence to set
	 */
	public void setLicence(Licence licence) {
		this.licence = licence;
	}
	/**
	 * @return the dateCreation
	 */
	public Date getDateCreation() {
		return dateCreation;
	}
	/**
	 * @param dateCreation the dateCreation to set
	 */
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	/**
	 * @return the transporteur
	 */
	@OneToOne
	@JoinColumn(name = "idtransporteur", nullable=false)
	public Societe getTransporteur() {
		return transporteur;
	}
	/**
	 * @param transporteur the transporteur to set
	 */
	public void setTransporteur(Societe transporteur) {
		this.transporteur = transporteur;
	}
	/**
	 * @return the userCreation
	 */
	@OneToOne
	@JoinColumn(name = "idusercreation", nullable=true)
	public Utilisateur getUserCreation() {
		return userCreation;
	}
	/**
	 * @param userCreation the userCreation to set
	 */
	public void setUserCreation(Utilisateur userCreation) {
		this.userCreation = userCreation;
	}
	

}
