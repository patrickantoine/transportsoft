package com.transport.services;

import java.io.Serializable;
import java.security.Key;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.app.util.FonctionsUtils;
import com.app.util.HibernateUtil;
import com.app.util.Login;
import com.transport.entity.Profil;
import com.transport.entity.ProfilRole;
import com.transport.entity.Role;
import com.transport.entity.Utilisateur;


@Named
@SessionScoped
public class ParametreService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ProjetService projetService;
	
	@Inject
	private ReferentielService referentielService;
	
	@Inject
	private Login login;
	
	private Utilisateur utilisateur = null;
	
	private List<Utilisateur> listUser = null;
	
	private List<Profil> listProfil = null;
	
	private Profil profil = null;
	
	private TreeNode arbreRole;
	private TreeNode[] roleChoisi;
	
	//***********************METHODES*****************************************
	
	//*****************UTILISATEUR
	@SuppressWarnings("unchecked")
	public void chargerProfil(){
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	setListProfil(session.createQuery("from Profil n order by n.libelle").list());
    	trans.commit();
	}
	
	public void initProfil() {
		profil = new Profil();
		profil.setListProfilRole(new ArrayList<ProfilRole>());
	}
	
	public void initUser() {
		utilisateur = new Utilisateur();
		utilisateur.setDateCreation(new Date());
		projetService.initPersonne();
	}
	
	public void saveUser() {
		projetService.savePersonne();
		utilisateur.setPersonne(projetService.getPersonne());
		try {
			if(utilisateur.getId() == null) {
				utilisateur.setMdp(encrypt("00000000", utilisateur.getLogin()));
			}
			
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	
	    	session.saveOrUpdate(utilisateur);
	    	trans.commit();
	    	
	    	initUser();
	    	chargerUser();
	    	referentielService.chargerUtilisateur();
	    	FonctionsUtils.popup("Utilisateur mis � jour...");
		}catch(Exception e) {
			FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
			e.printStackTrace();			
		}
	}
	
	public void majPasswordUser( Utilisateur us) {
		try {
			
			us.setMdp(encrypt(us.getMdpTemp(), us.getLogin()));
			
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	
	    	session.merge(us);
	    	trans.commit();
	    	
	    	FonctionsUtils.popup("Utilisateur mis � jour...");
		}catch(Exception e) {
			FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
			e.printStackTrace();			
		}
	}
	
	public void reinitialiserPasswordUser( Utilisateur us) {
		try {
			
			us.setMdp(encrypt("00000000", us.getLogin()));
			
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	
	    	session.merge(us);
	    	trans.commit();
	    	
	    	FonctionsUtils.popup("Utilisateur mis � jour...");
		}catch(Exception e) {
			FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
			e.printStackTrace();			
		}
	}
	
	public void modifierMotdepasse() {
		try {
			
			login.getUserConnecte().setMdp(encrypt(login.getUserConnecte().getMdpTemp(), login.getUserConnecte().getLogin()));
			
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	
	    	session.merge(login.getUserConnecte());
	    	trans.commit();
	    	
	    	FonctionsUtils.popup("Votre mot de passe a �t� modifi�...");
		}catch(Exception e) {
			FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
			e.printStackTrace();			
		}
	}
	
	public void reinitialiserPasswordAdmin() {
		
		try {
			
			
			
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	
	    	Utilisateur us = (Utilisateur) session.createQuery("from Utilisateur u where u.id = 1").uniqueResult();
	    			
	    	us.setMdp(encrypt("00000000", us.getLogin()));
	    	
	    	session.merge(us);
	    	trans.commit();
	    	
	    	FonctionsUtils.popup("Utilisateur mis � jour...");
		}catch(Exception e) {
			FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
			e.printStackTrace();			
		}
	}
	
	public String versListUser() {
		initUser();
		chargerUser();
		return "/private/user/listUser.xhtml?faces-redirect=true";
	}
	
	public String versProfil() {
		return "/private/user/profil.xhtml?faces-redirect=true";
	}
	
	public String versProfilUser() {
		initUser();
		return "/private/user/profilUser.xhtml?faces-redirect=true";
	}	
	
	public String versListProfil() {
		initProfil();
		//chargerProfil();
		chargerArbreRoleSansProfil();
		return "/private/user/listProfil.xhtml?faces-redirect=true";
	}	
	
	public Boolean habilitation(Long idMenu) {
		
		if(login.getHabilitation().contains(idMenu)) {
			return true;
		}else {
			return false;
		}
	}
	
    public void chargerArbreRoleSansProfil() {
    	ProfilRole lerole = null;
    	arbreRole = new DefaultTreeNode("ROLES", null);
    	
    	
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	
    	for(Role uo : referentielService.getListRole()) {
    		if(uo.getRoleParent() == null) {
    			TreeNode node0 = new DefaultTreeNode(uo, arbreRole);
    			chargerBrancheArbreSansProfilSession(session,uo,node0);
    		}
    	}
    	
    	trans.commit();
    }
    
    public void chargerArbreRoleSansProfilSession(Session session) {
    	ProfilRole lerole = null;
    	arbreRole = new DefaultTreeNode("ROLES", null);
    	
    	
    	for(Role uo : referentielService.getListRole()) {
    		if(uo.getRoleParent() == null) {
    			TreeNode node0 = new DefaultTreeNode(uo, arbreRole);
    			chargerBrancheArbreSansProfil(uo,node0);
    		}
    	}
    }
	
    public void chargerArbreRole() {
    	ProfilRole lerole = null;
    	arbreRole = new DefaultTreeNode("ROLES", null);
    	
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	for(Role uo : referentielService.getListRole()) {
    		if(uo.getRoleParent() == null) {
    			TreeNode node0 = new DefaultTreeNode(uo, arbreRole);
				
				  List<ProfilRole> list = session.createQuery("from ProfilRole pr where pr.profil = :param1 and pr.role = :param2") .setParameter("param1", profil) .setParameter("param2", uo).list();
				  if(!list.isEmpty()) { 
					  node0.setSelected(true); 
				  }
				 
    			chargerBrancheArbre(uo,node0);
    		}
    	}
    }
    
    @SuppressWarnings("unchecked")
	public void chargerBrancheArbreSession(Session session, Role un,TreeNode root) {
    	
    	ProfilRole lerole = null;
    	
    	List<Role> listSOU = session.createQuery("from Role u "
    			+ "where u.roleParent = :param ")
    			.setParameter("param", un)
    			.list();
    	
    	if(listSOU.size() > 0){
    	
    		for(Role u :listSOU){
    			TreeNode node0 = new DefaultTreeNode(u, root);
				List<ProfilRole> list = session.createQuery("from ProfilRole pr where pr.profil = :param1 and pr.role = :param2")
						.setParameter("param1", profil)
						.setParameter("param2", u).list();
				if(!list.isEmpty()) {
					node0.setSelected(true);
				}
    			chargerBrancheArbreSession(session,u,node0);
    		}
    	}
    }
    
    
    
    @SuppressWarnings("unchecked")
	public void chargerBrancheArbre(Role un,TreeNode root) {
    	
    	ProfilRole lerole = null;
    	
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	List<Role> listSOU = session.createQuery("from Role u "
    			+ "where u.roleParent = :param ")
    			.setParameter("param", un)
    			.list();
    	
    	if(listSOU.size() > 0){
    	
    		for(Role u :listSOU){
    			TreeNode node0 = new DefaultTreeNode(u, root);
				List<ProfilRole> list = session.createQuery("from ProfilRole pr where pr.profil = :param1 and pr.role = :param2")
						.setParameter("param1", profil)
						.setParameter("param2", u).list();
				if(!list.isEmpty()) {
					node0.setSelected(true);
				}
    			chargerBrancheArbre(u,node0);
    		}
    	}
    }

    @SuppressWarnings("unchecked")
	public void chargerBrancheArbreSansProfilSession(Session session,Role un,TreeNode root) {
    	
    	ProfilRole lerole = null;
    	
    	List<Role> listSOU = session.createQuery("from Role u "
    			+ "where u.roleParent = :param ")
    			.setParameter("param", un)
    			.list();
    	
    	if(listSOU.size() > 0){
    	
    		for(Role u :listSOU){
    			TreeNode node0 = new DefaultTreeNode(u, root);
    			chargerBrancheArbreSession(session,u,node0);
    		}
    	}
    }
    
    @SuppressWarnings("unchecked")
	public void chargerBrancheArbreSansProfil(Role un,TreeNode root) {
    	
    	ProfilRole lerole = null;
    	
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	List<Role> listSOU = session.createQuery("from Role u "
    			+ "where u.roleParent = :param ")
    			.setParameter("param", un)
    			.list();
    	
    	if(listSOU.size() > 0){
    	
    		for(Role u :listSOU){
    			TreeNode node0 = new DefaultTreeNode(u, root);
    			chargerBrancheArbre(u,node0);
    		}
    	}
    }
	
	@SuppressWarnings("unchecked")
	public void saveProfil() {
		try {
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	profil.setNbRoles(roleChoisi.length);
	    	if(profil.getId() != null) {
	    		session.createQuery("delete ProfilRole pr where pr.profil = :param").setParameter("param", profil).executeUpdate();
	    	}
	    	
	    	profil.setListProfilRole(new ArrayList<ProfilRole>());
	    	
	    	for(int u=0; u < roleChoisi.length; u++){
	    		ProfilRole pr = new ProfilRole();
	    		pr.setRole((Role) roleChoisi[u].getData());
	    		profil.getListProfilRole().add(pr);
	    	}
	    	
	    	if(profil.getId() != null) {
	    		session.merge(profil);
	    	}else {
	    		session.save(profil);
	    	}
	    	
	    	trans.commit();
	    	//chargerProfil();
	    	referentielService.chargerProfil();
	    	initProfil();
	    	chargerArbreRoleSansProfil();
	    	
	    	if(profil.getId() != null) {
	    		FonctionsUtils.popup("Profil mis � jour...");
	    	}else {
	    		FonctionsUtils.popup("Profil cr��...");
	    	}
	    	
	    	
		}catch(Exception e) {
			FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
			e.printStackTrace();			
		}
	}
	
	public void profilEncours(Profil pp) {
		try {
			this.setProfil(pp);
			chargerArbreRole();
			FonctionsUtils.popup("Profil charg�...");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	
    public void initArbreRole() {
    	arbreRole = new DefaultTreeNode("ROLES", null);
    	for(Role uo : referentielService.getListRole()) {
    		if(uo.getRoleParent() == null) {
    			TreeNode node0 = new DefaultTreeNode(uo, arbreRole);
        		creerBrancheArbre(uo,node0);
    		}
    	}
    }
    
    @SuppressWarnings("unchecked")
	public void creerBrancheArbre(Role un,TreeNode root) {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	List<Role> listSOU = session.createQuery("from Role u "
    			+ "where u.roleParent = :param ")
    			.setParameter("param", un)
    			.list();
    	
    	if(listSOU.size() > 0){
    	
    		for(Role u :listSOU){
    			TreeNode node0 = new DefaultTreeNode(u, root);
    			creerBrancheArbre(u,node0);
    		}
    	}
    }
	
	@SuppressWarnings("unchecked")
	public void chargerUser() {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listUser = session.createQuery("from Utilisateur u left join fetch u.personne pers order by pers.nom").list();
    	trans.commit();
	}
	public static String encrypt(String password,String key){
		try{
			Key clef = new SecretKeySpec(key.getBytes("ISO-8859-2"),"Blowfish");
			Cipher cipher=Cipher.getInstance("Blowfish");
			cipher.init(Cipher.ENCRYPT_MODE,clef);
			return new String(cipher.doFinal(password.getBytes()));
		}catch (Exception e){
			return null;
		}
	}

	/**
	 * @return the utilisateur
	 */
	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	/**
	 * @param utilisateur the utilisateur to set
	 */
	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	/**
	 * @return the listUser
	 */
	public List<Utilisateur> getListUser() {
		return listUser;
	}

	/**
	 * @param listUser the listUser to set
	 */
	public void setListUser(List<Utilisateur> listUser) {
		this.listUser = listUser;
	}
	
	
	/**
	 * @return the listProfil
	 */
	public List<Profil> getListProfil() {
		return listProfil;
	}


	/**
	 * @param listProfil the listProfil to set
	 */
	public void setListProfil(List<Profil> listProfil) {
		this.listProfil = listProfil;
	}

	/**
	 * @return the profil
	 */
	public Profil getProfil() {
		return profil;
	}

	/**
	 * @param profil the profil to set
	 */
	public void setProfil(Profil profil) {
		this.profil = profil;
	}

	/**
	 * @return the roleChoisi
	 */
	public TreeNode[] getRoleChoisi() {
		return roleChoisi;
	}

	/**
	 * @param roleChoisi the roleChoisi to set
	 */
	public void setRoleChoisi(TreeNode[] roleChoisi) {
		this.roleChoisi = roleChoisi;
	}

	public TreeNode getArbreRole() {
		return arbreRole;
	}

	public void setArbreRole(TreeNode arbreRole) {
		this.arbreRole = arbreRole;
	}
}
