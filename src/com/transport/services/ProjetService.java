package com.transport.services;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.app.util.FonctionsUtils;
import com.app.util.HibernateUtil;
import com.transport.entity.Adresse;
import com.transport.entity.Licence;
import com.transport.entity.Personne;
import com.transport.entity.Profil;
import com.transport.entity.Province;
import com.transport.entity.Societe;
import com.transport.entity.Type;
import com.transport.entity.Ville;

@Named
@SessionScoped
public class ProjetService implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Adresse adresse = null;
	private Personne personne = null;
	private Type marque = null;
	private Province province = null;
	private Type typeLicence = null;
	private Type typeActivite = null;
	private Date date1;
	private Long nbTransporteur = 0l;
	private Long nbVoiture = 0l;
	private Long nbLicenceActive = 0l;
	private Long nbLicencePerime = 0l;
	private List<Societe> listNouveauxTransporteur;
	private List<Licence> listNouvelleLicence;
	private List<Licence> listLicence2;
	private String nom;
	private String prenom;
	
	
	//*****************************************************
	

	
	@SuppressWarnings("unchecked")
	public void chargerInfo() {
		
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	listLicence2 = session.createQuery("from Licence l where l.active = true and DATEDIFF(l.dateFin,CURDATE()) < 30").list();
    	nbTransporteur = (Long) session.createQuery("select count(a) from Societe a where a.active = true").uniqueResult();
    	nbVoiture = (Long) session.createQuery("select count(a) from Voiture a where a.transporteur.active = true").uniqueResult();
    	nbLicenceActive = (Long) session.createQuery("select count(a) from Licence a where a.active = true and a.voiture.transporteur.active = true").uniqueResult();
    	nbLicencePerime = (Long) session.createQuery("select count(a) from Licence a where a.active = false and renouvelle = false and a.voiture.transporteur.active = true").uniqueResult();
    	//listNouveauxTransporteur = session.createQuery("from Societe s where s.active = true order by s.dateCreation desc").setMaxResults(4).list();
    	//listNouvelleLicence = session.createQuery("from Licence s where s.active = true and s.voiture.transporteur.active = true order by s.dateCreation desc").setMaxResults(4).list();
    	trans.commit();
	}
	
	
	public void savePersonne() {
		try {
		
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	
	    	session.saveOrUpdate(personne);
	    	trans.commit();
	    	
		}catch(Exception e) {
			FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
			e.printStackTrace();			
		}
	}
	
	public void saveAdresse() {
		try {
		
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	
	    	session.saveOrUpdate(adresse);
	    	trans.commit();
	    	
		}catch(Exception e) {
			FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
			e.printStackTrace();			
		}
	}
	
//*********************************************************
	public String versHome() {
		chargerInfo();
		return "/private/home.xhtml?faces-redirect=true";
	}
	
	public void initAdresse() {
		adresse = new Adresse();
		adresse.setVille(new Ville());
	}
	
	public void initPersonne() {
		personne = new Personne();
	}



//*********************************************************

	/**
	 * @return the adresse
	 */
	public Adresse getAdresse() {
		return adresse;
	}





	/**
	 * @param adresse the adresse to set
	 */
	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	/**
	 * @return the personne
	 */
	public Personne getPersonne() {
		return personne;
	}

	/**
	 * @param personne the personne to set
	 */
	public void setPersonne(Personne personne) {
		this.personne = personne;
	}

	/**
	 * @return the marque
	 */
	public Type getMarque() {
		return marque;
	}

	/**
	 * @param marque the marque to set
	 */
	public void setMarque(Type marque) {
		this.marque = marque;
	}

	/**
	 * @return the province
	 */
	public Province getProvince() {
		return province;
	}

	/**
	 * @param province the province to set
	 */
	public void setProvince(Province province) {
		this.province = province;
	}

	/**
	 * @return the nbTransporteur
	 */
	public Long getNbTransporteur() {
		return nbTransporteur;
	}

	/**
	 * @param nbTransporteur the nbTransporteur to set
	 */
	public void setNbTransporteur(Long nbTransporteur) {
		this.nbTransporteur = nbTransporteur;
	}

	/**
	 * @return the nbVoiture
	 */
	public Long getNbVoiture() {
		return nbVoiture;
	}

	/**
	 * @param nbVoiture the nbVoiture to set
	 */
	public void setNbVoiture(Long nbVoiture) {
		this.nbVoiture = nbVoiture;
	}

	/**
	 * @return the nbLicenceActive
	 */
	public Long getNbLicenceActive() {
		return nbLicenceActive;
	}

	/**
	 * @param nbLicenceActive the nbLicenceActive to set
	 */
	public void setNbLicenceActive(Long nbLicenceActive) {
		this.nbLicenceActive = nbLicenceActive;
	}

	/**
	 * @return the nbLicencePerime
	 */
	public Long getNbLicencePerime() {
		return nbLicencePerime;
	}

	/**
	 * @param nbLicencePerime the nbLicencePerime to set
	 */
	public void setNbLicencePerime(Long nbLicencePerime) {
		this.nbLicencePerime = nbLicencePerime;
	}

	/**
	 * @return the listNouveauxTransporteur
	 */
	public List<Societe> getListNouveauxTransporteur() {
		return listNouveauxTransporteur;
	}

	/**
	 * @param listNouveauxTransporteur the listNouveauxTransporteur to set
	 */
	public void setListNouveauxTransporteur(List<Societe> listNouveauxTransporteur) {
		this.listNouveauxTransporteur = listNouveauxTransporteur;
	}


	/**
	 * @return the listNouvelleLicence
	 */
	public List<Licence> getListNouvelleLicence() {
		return listNouvelleLicence;
	}


	/**
	 * @param listNouvelleLicence the listNouvelleLicence to set
	 */
	public void setListNouvelleLicence(List<Licence> listNouvelleLicence) {
		this.listNouvelleLicence = listNouvelleLicence;
	}


	/**
	 * @return the typeLicence
	 */
	public Type getTypeLicence() {
		return typeLicence;
	}


	/**
	 * @param typeLicence the typeLicence to set
	 */
	public void setTypeLicence(Type typeLicence) {
		this.typeLicence = typeLicence;
	}


	/**
	 * @return the typeActivite
	 */
	public Type getTypeActivite() {
		return typeActivite;
	}


	/**
	 * @param typeActivite the typeActivite to set
	 */
	public void setTypeActivite(Type typeActivite) {
		this.typeActivite = typeActivite;
	}


	/**
	 * @return the date1
	 */
	public Date getDate1() {
		return date1;
	}


	/**
	 * @param date1 the date1 to set
	 */
	public void setDate1(Date date1) {
		this.date1 = date1;
	}


	/**
	 * @return the nom
	 */
	public String getNom() {
		return nom;
	}


	/**
	 * @param nom the nom to set
	 */
	public void setNom(String nom) {
		this.nom = nom;
	}


	/**
	 * @return the prenom
	 */
	public String getPrenom() {
		return prenom;
	}


	/**
	 * @param prenom the prenom to set
	 */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public List<Licence> getListLicence2() {
		return listLicence2;
	}


	public void setListLicence2(List<Licence> listLicence2) {
		this.listLicence2 = listLicence2;
	}






}
