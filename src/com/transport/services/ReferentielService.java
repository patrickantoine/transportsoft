package com.transport.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.inject.Named;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.app.util.HibernateUtil;
import com.transport.entity.Profil;
import com.transport.entity.Province;
import com.transport.entity.Role;
import com.transport.entity.Type;
import com.transport.entity.Utilisateur;
import com.transport.entity.Ville;

@Named
@ApplicationScoped
public class ReferentielService implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private List<Type> listType = new ArrayList<Type>();
	private List<Type> listTypeActivite = new ArrayList<Type>();
	private List<Type> listTypeFormeJurique = new ArrayList<Type>();
	private List<Type> listTypeMarque = new ArrayList<Type>();
	private List<Type> listTypeModele = new ArrayList<Type>();
	private List<Type> listTypeMotorisation = new ArrayList<Type>();
	private List<Type> listTypeLicence = new ArrayList<Type>();
	private List<Province> listProvince = new ArrayList<Province>();
	private List<Ville> listVille = new ArrayList<Ville>();
	private List<Role> listRole = new ArrayList<Role>() ;
	private List<Profil> listProfil = new ArrayList<Profil>() ;
	private List<Utilisateur> listUtilisateur = new ArrayList<Utilisateur>() ;
	
	//**************************************************************
	public void init(@Observes @Initialized(ApplicationScoped.class) Object init){
		chargerType();
		chargerTypeActivite();
		chargerTypeFormeJuridique();
		chargerMarque();
		chargerModele();
		chargerMotorisation();
		chargerLicence();
		chargerProvince();
		chargerVille();
		chargerRole();
		chargerProfil();
		chargerUtilisateur();
	}
	
	//**************************************************************
	@SuppressWarnings("unchecked")
	public void chargerUtilisateur(){
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	listUtilisateur = session.createQuery("from Utilisateur n order by n.personne.nom asc").list();
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerProfil(){
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	listProfil = session.createQuery("from Profil n order by n.libelle asc").list();
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerRole(){
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	listRole = session.createQuery("from Role n order by n.libelle asc").list();
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerVille() {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listVille = session.createQuery(" from Ville c order by c.libelle").list();
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerProvince() {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listProvince = session.createQuery("from Province c order by c.libelle").list();
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerMarque() {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listTypeMarque = session.createQuery("from Type c where c.contextMarque = true  order by c.libelle").list();
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerModele() {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listTypeModele = session.createQuery("from Type c where c.contextModele = true  order by c.libelle").list();
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerMotorisation() {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listTypeMotorisation = session.createQuery("from Type c where c.contextMotorisation = true  order by c.libelle").list();
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerLicence() {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listTypeLicence = session.createQuery("from Type c where c.contextLicence = true  order by c.montant asc").list();
    	trans.commit();
	}
	
	public Type returnType(Long id){
		Type objet =null;	
		for(Type it:listType){
			if(it.getId().equals(id)){
				objet = it;
			}
		}
		return objet;
	}
	
	public Profil returnProfil(Long id){
		Profil objet =null;	
		for(Profil it:listProfil){
			if(it.getId().equals(id)){
				objet = it;
			}
		}
		return objet;
	}
	
	public Utilisateur returnUtilisateur(Long id){
		Utilisateur objet =null;	
		for(Utilisateur it:listUtilisateur){
			if(it.getId().equals(id)){
				objet = it;
			}
		}
		return objet;
	}
	
	public Province returnProvince(Long id){
		Province objet =null;	
		for(Province it:listProvince){
			if(it.getId().equals(id)){
				objet = it;
			}
		}
		return objet;
	}
	
	public Ville returnVille(Long id){
		Ville objet =null;	
		for(Ville it:listVille){
			if(it.getId().equals(id)){
				objet = it;
			}
		}
		return objet;
	}
	
	@SuppressWarnings("unchecked")
	public void chargerType() {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listType = session.createQuery("from Type c order by c.libelle").list();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerTypeActivite() {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listTypeActivite = session.createQuery("from Type c where c.contextActivite = true  order by c.libelle").list();
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerTypeFormeJuridique() {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listTypeFormeJurique = session.createQuery("from Type c where c.contextJuridique = true  order by c.libelle").list();
    	trans.commit();
	}



	/**
	 * @return the listType
	 */
	public List<Type> getListType() {
		return listType;
	}




	/**
	 * @param listType the listType to set
	 */
	public void setListType(List<Type> listType) {
		this.listType = listType;
	}

	/**
	 * @return the listTypeActivite
	 */
	public List<Type> getListTypeActivite() {
		return listTypeActivite;
	}

	/**
	 * @param listTypeActivite the listTypeActivite to set
	 */
	public void setListTypeActivite(List<Type> listTypeActivite) {
		this.listTypeActivite = listTypeActivite;
	}

	/**
	 * @return the listTypeFormeJurique
	 */
	public List<Type> getListTypeFormeJurique() {
		return listTypeFormeJurique;
	}

	/**
	 * @param listTypeFormeJurique the listTypeFormeJurique to set
	 */
	public void setListTypeFormeJurique(List<Type> listTypeFormeJurique) {
		this.listTypeFormeJurique = listTypeFormeJurique;
	}

	/**
	 * @return the listTypeMarque
	 */
	public List<Type> getListTypeMarque() {
		return listTypeMarque;
	}

	/**
	 * @param listTypeMarque the listTypeMarque to set
	 */
	public void setListTypeMarque(List<Type> listTypeMarque) {
		this.listTypeMarque = listTypeMarque;
	}

	/**
	 * @return the listTypeModele
	 */
	public List<Type> getListTypeModele() {
		return listTypeModele;
	}

	/**
	 * @param listTypeModele the listTypeModele to set
	 */
	public void setListTypeModele(List<Type> listTypeModele) {
		this.listTypeModele = listTypeModele;
	}

	/**
	 * @return the listTypeMotorisation
	 */
	public List<Type> getListTypeMotorisation() {
		return listTypeMotorisation;
	}

	/**
	 * @param listTypeMotorisation the listTypeMotorisation to set
	 */
	public void setListTypeMotorisation(List<Type> listTypeMotorisation) {
		this.listTypeMotorisation = listTypeMotorisation;
	}

	/**
	 * @return the listTypeLicence
	 */
	public List<Type> getListTypeLicence() {
		return listTypeLicence;
	}

	/**
	 * @param listTypeLicence the listTypeLicence to set
	 */
	public void setListTypeLicence(List<Type> listTypeLicence) {
		this.listTypeLicence = listTypeLicence;
	}

	/**
	 * @return the listProvince
	 */
	public List<Province> getListProvince() {
		return listProvince;
	}

	/**
	 * @param listProvince the listProvince to set
	 */
	public void setListProvince(List<Province> listProvince) {
		this.listProvince = listProvince;
	}

	/**
	 * @return the listVille
	 */
	public List<Ville> getListVille() {
		return listVille;
	}

	/**
	 * @param listVille the listVille to set
	 */
	public void setListVille(List<Ville> listVille) {
		this.listVille = listVille;
	}

	/**
	 * @return the listRole
	 */
	public List<Role> getListRole() {
		return listRole;
	}

	/**
	 * @param listRole the listRole to set
	 */
	public void setListRole(List<Role> listRole) {
		this.listRole = listRole;
	}

	/**
	 * @return the listProfil
	 */
	public List<Profil> getListProfil() {
		return listProfil;
	}

	/**
	 * @param listProfil the listProfil to set
	 */
	public void setListProfil(List<Profil> listProfil) {
		this.listProfil = listProfil;
	}

	public List<Utilisateur> getListUtilisateur() {
		return listUtilisateur;
	}

	public void setListUtilisateur(List<Utilisateur> listUtilisateur) {
		this.listUtilisateur = listUtilisateur;
	}

	
}
