package com.transport.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.app.util.FonctionsUtils;
import com.app.util.HibernateUtil;
import com.transport.entity.Province;
import com.transport.entity.StatEntity;
import com.transport.entity.Type;
import com.transport.entity.Utilisateur;
import com.transport.entity.Ville;

@Named
@SessionScoped
public class StatService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ReferentielService referentielService;
	
	@Inject
	private VoitureService voitureService;
	
	private List<StatEntity> listStat = null;
	private List<StatEntity> listStat2 = null;
	private List<StatEntity> listStat3 = null;
	private List<StatEntity> listStat4 = null;
	private List<StatEntity> listStat5 = null;
	
	private Date date1;
	private Date date2;
	
	//*******************************************************
	
	public void chargerStatAgenceSaisie(){
		Date dateParcour = date1;
		listStat = new ArrayList<StatEntity>();
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
		
		List<Utilisateur> listGestionnaire = session.createQuery("select distinct t.gestionnaire from Societe t").list();
		
/*		do{
			
			
			
			dateParcour = FonctionsUtils.addNdays(dateParcour, 1);
		}while(FonctionsUtils.date1AfterDate2(date2, dateParcour));*/
		
		for(Utilisateur iu : listGestionnaire){
	    	//Double montant = (Double) session.createQuery("select sum(a.typeLicence.montant) from Licence a where a.active = true and a.voiture.transporteur.active = true and a.voiture.transporteur.gestionnaire = :param").setParameter("param", iu).uniqueResult();
	    	Long nbTransporteur = (Long) session.createQuery("select count(a) from Societe a where a.dateCreation >= :param2 and a.dateCreation <= :param3 and a.userCreation= :param")
	    							.setParameter("param", iu)
	    							.setParameter("param2", date1)
	    							.setParameter("param3", date2)
	    							.uniqueResult();
	    	
	    	StatEntity st = new StatEntity(iu.getPersonne().getNomComplet(), nbTransporteur*1d, null,"" );
			listStat.add(st);
		}
		trans.commit();
		FonctionsUtils.popup("Chargement termin�...");
	}
	
	public void chargerStatGestionnaire(){
		listStat = new ArrayList<StatEntity>();
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
		
		List<Utilisateur> listGestionnaire = session.createQuery("select distinct t.gestionnaire from Societe t").list();
		
		for(Utilisateur iu : listGestionnaire){
	    	Double montant = (Double) session.createQuery("select sum(a.typeLicence.montant) from Licence a where a.active = true and a.voiture.transporteur.active = true and a.voiture.transporteur.gestionnaire = :param").setParameter("param", iu).uniqueResult();
	    	Long nbTransporteur = (Long) session.createQuery("select count(a) from Societe a where a.active = true and a.gestionnaire= :param").setParameter("param", iu).uniqueResult();
	    	if(montant == null){
	    		montant = 0d;
	    	}
	    	StatEntity st = new StatEntity(iu.getPersonne().getNomComplet(), montant, null, nbTransporteur+"");
			listStat.add(st);
		}
		
		trans.commit();
	}
	
	public void chargerStatVille(){
		listStat2 = new ArrayList<StatEntity>();
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
		
		for(Ville iu : referentielService.getListVille()){
	    	Double montant = (Double) session.createQuery("select sum(a.typeLicence.montant) from Licence a where a.active = true and a.voiture.transporteur.active = true and a.voiture.transporteur.adresse.ville= :param").setParameter("param", iu).uniqueResult();
	    	if(montant == null){
	    		montant = 0d;
	    	}
	    	Long nbTransporteur = (Long) session.createQuery("select count(a) from Societe a where a.active = true and a.adresse.ville= :param").setParameter("param", iu).uniqueResult();
	    	StatEntity st = new StatEntity(iu.getLibelle(), montant, null, nbTransporteur+"");
			listStat2.add(st);
		}
		
		trans.commit();
	}
	
	public void chargerStatProvince(){
		listStat3 = new ArrayList<StatEntity>();
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
		
		for(Province iu : referentielService.getListProvince()){
	    	Double montant = (Double) session.createQuery("select sum(a.typeLicence.montant) from Licence a where a.active = true and a.voiture.transporteur.active = true and a.voiture.transporteur.adresse.ville.province= :param").setParameter("param", iu).uniqueResult();
	    	if(montant == null){
	    		montant = 0d;
	    	}
	    	Long nbTransporteur = (Long) session.createQuery("select count(a) from Societe a where a.active = true and a.adresse.ville.province= :param").setParameter("param", iu).uniqueResult();
	    	StatEntity st = new StatEntity(iu.getLibelle(), montant, null, nbTransporteur+"");
			listStat3.add(st);
		}
		
		trans.commit();
	}
	
	public void chargerStatActivite(){
		listStat4 = new ArrayList<StatEntity>();
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
		
		for(Type iu : referentielService.getListTypeActivite()){
	    	Double montant = (Double) session.createQuery("select sum(a.typeLicence.montant) from Licence a where a.active = true and a.voiture.transporteur.active = true and a.typeActivite= :param").setParameter("param", iu).uniqueResult();
	    	if(montant == null){
	    		montant = 0d;
	    	}
	    	Long nbTransporteur = (Long) session.createQuery("select count(distinct a.voiture.transporteur) from Licence a where a.active = true and a.voiture.transporteur.active= true and a.typeActivite= :param").setParameter("param", iu).uniqueResult();
	    	StatEntity st = new StatEntity(iu.getLibelle(), montant, null, nbTransporteur+"");
			listStat4.add(st);
		}
		
		trans.commit();
	}
	
	public void chargerStatModeleVoiture(){
		listStat5 = new ArrayList<StatEntity>();
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
		
		for(Type iu : referentielService.getListTypeModele()){
	    	Double montant = (Double) session.createQuery("select sum(a.typeLicence.montant) from Licence a where a.active = true and a.voiture.transporteur.active = true and a.voiture.modele = :param").setParameter("param", iu).uniqueResult();
	    	if(montant == null){
	    		montant = 0d;
	    	}
	    	Long nbTransporteur = (Long) session.createQuery("select count(distinct a.transporteur) from Voiture a where a.transporteur.active = true and a.modele = :param").setParameter("param", iu).uniqueResult();
	    	StatEntity st = new StatEntity(iu.getTypeParent().getLibelle()+" "+iu.getLibelle(), montant, null, nbTransporteur+"");
			listStat5.add(st);
		}
		
		trans.commit();
	}
	
	public String versAnalyseFinanciere() {
		initListes();
		chargerStatGestionnaire();
		chargerStatProvince();
		chargerStatVille();
		chargerStatActivite();
		chargerStatModeleVoiture();
		voitureService.chargerMontantLicenceActiveGlobal();
		voitureService.chargerMontantLicenceExpireeGlobal();
		return "/private/licence/stat.xhtml?faces-redirect=true";
	}
	
	public String versActiviteGestionnaire() {
		initListes();
		return "/private/licence/stat2.xhtml?faces-redirect=true";
	}
	
	public void initListes(){
		listStat = new ArrayList<StatEntity>();
		listStat2 = new ArrayList<StatEntity>();
		listStat3 = new ArrayList<StatEntity>();
		listStat4 = new ArrayList<StatEntity>();
		listStat5 = new ArrayList<StatEntity>();		
	}
	
	
	
	
	
	//*******************************************************
	

	public List<StatEntity> getListStat() {
		return listStat;
	}

	public void setListStat(List<StatEntity> listStat) {
		this.listStat = listStat;
	}

	public List<StatEntity> getListStat2() {
		return listStat2;
	}

	public void setListStat2(List<StatEntity> listStat2) {
		this.listStat2 = listStat2;
	}

	public List<StatEntity> getListStat3() {
		return listStat3;
	}

	public void setListStat3(List<StatEntity> listStat3) {
		this.listStat3 = listStat3;
	}

	public List<StatEntity> getListStat4() {
		return listStat4;
	}

	public void setListStat4(List<StatEntity> listStat4) {
		this.listStat4 = listStat4;
	}

	public List<StatEntity> getListStat5() {
		return listStat5;
	}

	public void setListStat5(List<StatEntity> listStat5) {
		this.listStat5 = listStat5;
	}

	public Date getDate1() {
		return date1;
	}

	public void setDate1(Date date1) {
		this.date1 = date1;
	}

	public Date getDate2() {
		return date2;
	}

	public void setDate2(Date date2) {
		this.date2 = date2;
	}

}
