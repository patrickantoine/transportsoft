package com.transport.services;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.primefaces.context.RequestContext;

import com.app.util.FonctionsUtils;
import com.app.util.HibernateUtil;
import com.app.util.Login;
import com.app.util.PrintService;
import com.transport.entity.Adresse;
import com.transport.entity.Licence;
import com.transport.entity.Personne;
import com.transport.entity.Societe;
import com.transport.entity.SocieteActivite;
import com.transport.entity.Type;
import com.transport.entity.Utilisateur;

import net.sf.jasperreports.engine.JRException;

@Named
@SessionScoped
public class TransporteurService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private ProjetService projetService;
	
	@Inject
	private ReferentielService referentielService;
	
	@Inject
	private ParametreService parametreService;
	
	@Inject
	private Login login;
	
	@Inject
	private VoitureService voitureService;
	
	private Societe transporteur = null;
	private List<Societe> listTransporteur = null;
	private List<Societe> listTransporteur2 = null;
	private List<Societe> listTransporteur3 = null;
	private List<Societe> listTransporteur4 = null;
	private Utilisateur gestionnaire= null;
	
	//**********************************************************
	
	public void initTransporteur() {
		transporteur = new Societe();
		transporteur.setAdresse(new Adresse());
		transporteur.setGerant(new Personne());
		transporteur.setDateCreation(new Date());
		transporteur.setUserCreation(login.getUserConnecte());
		projetService.initAdresse();
		projetService.initPersonne();
		
	}
	
	@SuppressWarnings("unchecked")
	public void chargerTransporteurPortefeuilleActif() {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	listTransporteur = session.createQuery("from Societe s where s.active = true and s.gestionnaire = :param order by s.libelle ").setParameter("param", login.getUserConnecte()).list();
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerTransporteurPortefeuilleInactif() {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	listTransporteur2 = session.createQuery("from Societe s where s.active = false and s.gestionnaire = :param order by s.libelle ").setParameter("param", login.getUserConnecte()).list();
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerTransporteurPortefeuille() {
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	listTransporteur3 = session.createQuery("from Societe s where s.gestionnaire = :param order by s.libelle ").setParameter("param", login.getUserConnecte()).list();
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerTransporteur() {
		try{
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	
	    	listTransporteur = session.createQuery("from Societe s where s.active = true order by s.libelle ").list();
	    	trans.commit();
		}catch(Exception e){
    		e.printStackTrace();
    		FonctionsUtils.popupErreur("Erreur durant l'execution de la requ�te...");
    	}
		
	}
	
	@SuppressWarnings("unchecked")
	public void chargerTransporteurNonAffecteur() {
		
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listTransporteur4 = session.createQuery("from Societe s where s.affecter = false order by s.libelle ").list();
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerTousTransporteur() {
		
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listTransporteur2 = session.createQuery("from Societe s order by s.libelle ").list();
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerTransporteurSuspendu() {
		
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listTransporteur2 = session.createQuery("from Societe s where s.active = false order by s.libelle ").list();
    	trans.commit();
	}
	
	
	@SuppressWarnings("unchecked")
	public void chargerTransporteurAffecter() {
		
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listTransporteur3 = session.createQuery("from Societe s where s.affecter = true order by s.libelle ").list();
    	trans.commit();
	}
	
	public void chercherTransporteur() throws IOException{
		int param = 0;
		
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	Criteria criteria = session.createCriteria(Societe.class);
    	
    	criteria.createAlias("gerant", "pers",JoinType.LEFT_OUTER_JOIN);
    	
    	if(projetService.getNom().length() > 0) {
    		criteria.add(Restrictions.like("pers.nom", "%"+projetService.getNom()+"%" ));param++;
    	}
    	
    	if(projetService.getPrenom().length() > 0) {
    		criteria.add(Restrictions.like("pers.prenom", "%"+projetService.getPrenom()+"%" ));param++;
    	}
    	
    	if(transporteur.getDateCreation() != null && projetService.getDate1() != null) {
    		criteria.add(Restrictions.between("dateCreation", transporteur.getDateCreation(), projetService.getDate1()));param++;
    	}
    	
    	if(gestionnaire != null){
    		criteria.add(Restrictions.eq("gestionnaire", gestionnaire ));param++;
    	}
    	
    	if(projetService.getTypeActivite() != null){
    		List<Long> list = session.createQuery("select lic.voiture.transporteur.id from Licence lic where lic.typeActivite = :param").setParameter("param", projetService.getTypeActivite()).list();
    		criteria.add(Restrictions.in("id", list));
    		param++;    		
    	}
    	
    	criteria.add(Restrictions.eq("active", transporteur.getActive()));
    	
    	if(param > 0) {
        	listTransporteur = criteria.list();
        	
        	if(listTransporteur.isEmpty()) {
        		FonctionsUtils.popupWarning("Aucun r�sultat");
        	}else {
        		imprimeRechercheTransporteur();
        		FonctionsUtils.popup(listTransporteur.size()+" transporteur(s) trouv�(s)");
        	}
    	}else {
    		FonctionsUtils.popupWarning("Veuillez renseigner au moins un param�tre...");
    	}
    	
    	trans.commit();
    	
	}
	

	
	@SuppressWarnings("unchecked")
	public void saveTransporteur() {
		
		projetService.saveAdresse();
		projetService.savePersonne();
		transporteur.setAdresse(projetService.getAdresse());
		transporteur.setGerant(projetService.getPersonne());
		transporteur.setAffecter(true);
		transporteur.setGestionnaire(login.getUserConnecte());
		
		try {
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	
	    	List<Societe> list  = session.createQuery("from Societe s where TRIM(s.libelle) = :param").setParameter("param", transporteur.getLibelle().trim()).list();
	    	if(list.isEmpty()){
		    	session.saveOrUpdate(transporteur);
		    	trans.commit();
		    	projetService.chargerInfo();
		    	FonctionsUtils.popup("Transporteur mis � jour...");
	    	}else{
	    		FonctionsUtils.warn("Ce transporteur existe d�j�...");
	    	}
	    	
	    	trans.commit();

		}catch(Exception e) {
			FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
			e.printStackTrace();			
		}
	}
	
	public void updateTransporteur() {
		transporteur.setDateModif(new Date());
		transporteur.setUserModif(login.getUserConnecte());
		try {
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	session.merge(transporteur);
	    	session.merge(projetService.getPersonne());
	    	session.merge(projetService.getAdresse());
	    	trans.commit();
	    	projetService.chargerInfo();
	    	FonctionsUtils.popup("Transporteur mis � jour...");
		}catch(Exception e) {
			FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
			e.printStackTrace();			
		}
	}
	
	public void suspendreTransporteur() {
		if(transporteur != null && transporteur.getId() != null) {
			transporteur.setActive(false);
			transporteur.setUserModif(login.getUserConnecte());
			transporteur.setDateModif(new Date());
			try {
		    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		    	Transaction trans = session.beginTransaction();
		    	trans.begin();
		    	session.merge(transporteur);
		    	trans.commit();
		    	chargerTransporteur();
		    	chargerTransporteurSuspendu();
		    	projetService.chargerInfo();
		    	FonctionsUtils.popup("Transporteur suspendu...");
			}catch(Exception e) {
				FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
				e.printStackTrace();			
			}
		}else {
			FonctionsUtils.popupWarning("Veuillez choisir le transporteur a suspendre...");
		}
	}
	
	
	public void affecterTransporteur() {
		if(transporteur != null && transporteur.getId() != null) {
			transporteur.setAffecter(true);
			transporteur.setUserModif(login.getUserConnecte());
			transporteur.setGestionnaire(gestionnaire);
			transporteur.setDateModif(new Date());
			transporteur.setDateAffectation(new Date());
			try {
		    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		    	Transaction trans = session.beginTransaction();
		    	trans.begin();
		    	session.merge(transporteur);
		    	trans.commit();
		    	chargerTransporteurNonAffecteur();
		    	chargerTransporteurAffecter();
		    	projetService.chargerInfo();
		    	imprimeAffectationTransporteur();
		    	FonctionsUtils.popup("Transporteur affect�..."+gestionnaire.getPersonne().getNomComplet());
			}catch(Exception e) {
				FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
				e.printStackTrace();			
			}
		}else {
			FonctionsUtils.popupWarning("Veuillez choisir le transporteur � affecter au gestionnaire...");
		}
	}
	
	public void imprimeListe() throws IOException {
		ExternalContext ec =	FacesContext.getCurrentInstance().getExternalContext();
		ServletContext sc = (ServletContext)ec.getContext();
		
		String path = sc.getRealPath("/private/etats/");
		 Map parameters = new HashMap();
		 
			try {
				PrintService.genererEtat(null, "listTransporteur", "listTransporteur.pdf", parameters,"listTransporteur.pdf");
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			RequestContext.getCurrentInstance().execute("apercuPrint()");
	}
	
	@SuppressWarnings("unchecked")
	public void imprimeRechercheTransporteur() throws IOException {
		
		 Map parameters = new HashMap();
		 
		 if(transporteur.getDateCreation() != null && projetService.getDate1() != null) {
			 parameters.put("DEBUT", FonctionsUtils.formateDate(transporteur.getDateCreation(), "dd/MM/yyyy"));
			 parameters.put("FIN", FonctionsUtils.formateDate(projetService.getDate1(), "dd/MM/yyyy"));
		 }
		 
		 if(transporteur.getActive()) {
			 parameters.put("ACTIF", "Oui");
		 }else {
			 parameters.put("ACTIF", "Non");
		 }

		 List<Long> listID = new ArrayList<Long>();
		 
		 for(Societe iv : listTransporteur) {
			 listID.add(iv.getId());
		 }
		 parameters.put("LISTID", listID);
		 
		 try {
			PrintService.genererEtat(null, "resultatTransporteur", "resultatTransporteur.pdf", parameters,"resultatTransporteur.pdf");
		 } catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
	}
	
	
	@SuppressWarnings("unchecked")
	public void imprimeAffectationTransporteur() throws IOException {
		
		 Map parameters = new HashMap();
		 
		 if(transporteur.getDateCreation() != null && projetService.getDate1() != null) {
			 parameters.put("DEBUT", FonctionsUtils.formateDate(transporteur.getDateCreation(), "dd/MM/yyyy"));
			 parameters.put("FIN", FonctionsUtils.formateDate(projetService.getDate1(), "dd/MM/yyyy"));
		 }
		 
		 if(transporteur.getActive()) {
			 parameters.put("ACTIF", "Oui");
		 }else {
			 parameters.put("ACTIF", "Non");
		 }

		 List<Long> listID = new ArrayList<Long>();
		 
		 for(Societe iv : listTransporteur3) {
			 listID.add(iv.getId());
		 }
		 parameters.put("LISTID", listID);
		 
		 try {
			PrintService.genererEtat(null, "affectationTransporteur", "affectationTransporteur.pdf", parameters,"affectationTransporteur.pdf");
		 } catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
	}
	
	public void imprimeListeSuspendu() throws IOException {
		ExternalContext ec =	FacesContext.getCurrentInstance().getExternalContext();
		ServletContext sc = (ServletContext)ec.getContext();
		
		String path = sc.getRealPath("/private/etats/");
		 Map parameters = new HashMap();
		 
			try {
				PrintService.genererEtat(null, "listTransporteurSuspendu", "listTransporteurSuspendu.pdf", parameters,"listTransporteurSuspendu.pdf");
			} catch (JRException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	
	public String versPortefeuille() {
		chargerTransporteurPortefeuille();
		chargerTransporteurPortefeuilleActif();
		chargerTransporteurPortefeuilleInactif();
		voitureService.chargerVoiturePortefeuille();
		voitureService.chargerLicencePerimePortefeuille();
		voitureService.chargerMontantLicenceActivePortefeuille();
		voitureService.chargerMontantLicenceExpireePortefeuille();
		voitureService.chargerLicenceExiprantBientotPortefeuille();
		return "/private/transporteur/portefeuille.xhtml?faces-redirect=true";
	}
	
	public String versSuspendreTransporteur() {
		chargerTransporteur();
		initTransporteur();
		chargerTransporteurSuspendu();
		return "/private/transporteur/suspendreTransporteur.xhtml?faces-redirect=true";
	}

	public String versAffecterTransporteur() throws IOException {
		//chargerTransporteurNonAffecteur();
		chargerTousTransporteur();
		initTransporteur();
		chargerTransporteurAffecter();
		imprimeAffectationTransporteur();
		//parametreService.chargerUser();
		return "/private/transporteur/affecterTransporteur.xhtml?faces-redirect=true";
	}
	
	public String versAddTransporteur() {
		initTransporteur();
		return "/private/transporteur/addTransporteur.xhtml?faces-redirect=true";
	}
	
	public String versMajTransporteur() {
		chargerTousTransporteur();
		initTransporteur();
		return "/private/transporteur/majTransporteur.xhtml?faces-redirect=true";
	}
	
	public String versMajTransporteurPortefeuille() {
		return "/private/transporteur/majTransporteurPortefeuille.xhtml?faces-redirect=true";
	}
	
	public String versListeTransporteur() throws IOException {
		chargerTransporteur();
		imprimeListe();
		return "/private/transporteur/listTransporteur.xhtml?faces-redirect=true";
	}
	
	public String versRechercheTransporteur() {
		this.setGestionnaire(null);
		listTransporteur = new ArrayList<Societe>();
		initTransporteur();
		transporteur.setDateCreation(null);
		projetService.setDate1(null);
		return "/private/transporteur/searchTransporteur.xhtml?faces-redirect=true";
	}
	
	public String versFicheTransporteur(Societe s) {

		chargerInfoTransporteur(s);
		
		return "/private/transporteur/ficheTransporteur.xhtml?faces-redirect=true";
	}
	
	public void chargerInfoTransporteur(Societe s){
		try{
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	
	    	transporteur = (Societe) session.createQuery("from Societe s left outer join fetch s.listVoiture where s = :param").setParameter("param", s).uniqueResult();
	    	trans.commit();
		}catch(Exception e){
    		e.printStackTrace();
    		FonctionsUtils.popupErreur("Erreur durant l'execution de la requ�te...");
    	}
	}
	
	public String versFicheAffectationTransporteur(Societe s) {
		chargerInfoTransporteur(s);
		return "/private/transporteur/ficheAffectationTransporteur.xhtml?faces-redirect=true";
	}
	
	
	
	//**********************************************************
	/**
	 * @return the transporteur
	 */
	public Societe getTransporteur() {
		return transporteur;
	}

	/**
	 * @param transporteur the transporteur to set
	 */
	public void setTransporteur(Societe transporteur) {
		this.transporteur = transporteur;
	}

	/**
	 * @return the listTransporteur
	 */
	public List<Societe> getListTransporteur() {
		return listTransporteur;
	}

	/**
	 * @param listTransporteur the listTransporteur to set
	 */
	public void setListTransporteur(List<Societe> listTransporteur) {
		this.listTransporteur = listTransporteur;
	}

	/**
	 * @return the listTransporteur2
	 */
	public List<Societe> getListTransporteur2() {
		return listTransporteur2;
	}

	/**
	 * @param listTransporteur2 the listTransporteur2 to set
	 */
	public void setListTransporteur2(List<Societe> listTransporteur2) {
		this.listTransporteur2 = listTransporteur2;
	}

	public Utilisateur getGestionnaire() {
		return gestionnaire;
	}

	public void setGestionnaire(Utilisateur gestionnaire) {
		this.gestionnaire = gestionnaire;
	}

	public List<Societe> getListTransporteur3() {
		return listTransporteur3;
	}

	public void setListTransporteur3(List<Societe> listTransporteur3) {
		this.listTransporteur3 = listTransporteur3;
	}

	public List<Societe> getListTransporteur4() {
		return listTransporteur4;
	}

	public void setListTransporteur4(List<Societe> listTransporteur4) {
		this.listTransporteur4 = listTransporteur4;
	}
	
	

}
