package com.transport.services;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.app.util.FonctionsUtils;
import com.app.util.HibernateUtil;
import com.transport.entity.Type;
import com.transport.entity.Voiture;


@Named
@SessionScoped
public class TypeService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Inject
	private ReferentielService referentielService;
	private Type type = null;
	private List<Type> listtype = null;
	private Boolean context=false;
	
	
	//***************************************************************
	
	public void charger() {
		referentielService.chargerProvince();
		referentielService.chargerTypeActivite();
		referentielService.chargerTypeActivite();
		referentielService.chargerTypeFormeJuridique();
		referentielService.chargerVille();
		referentielService.chargerProvince();
		referentielService.chargerMarque();
		referentielService.chargerModele();
		referentielService.chargerType();
	}
	
	public void initType() {
		type = new Type();
		setContext(false);
		
	}
	
	public String versAddNatureActivite() {
		initType();
		setContext(true);
		type.setContextActivite(context);
		return "/private/type/addNatureActivite.xhtml?faces-redirect=true";
	}
	
	public String versAddMarque() {
		initType();
		setContext(true);
		this.type.setContextMarque(context);
		return "/private/type/addMarque.xhtml?faces-redirect=true";
	}
	public String versAddModele() {
		initType();
		setContext(true);
		this.type.setContextModele(context);
		return "/private/type/addModele.xhtml?faces-redirect=true";
	}
	public String versAddTypeSociete() {
		initType();
		setContext(true);
		type.setContextJuridique(context);
		return "/private/type/addTypeSociete.xhtml?faces-redirect=true";
	}
	
	
	public void saveType() {
		
		String typeMaj = "none";
		
		if(type.getContextActivite()){
			typeMaj = "act";
		}else if(type.getContextJuridique()){
			typeMaj = "jur";
		}else if(type.getContextMarque()){
			typeMaj = "marq";
		}else if(type.getContextModele()){
			typeMaj = "mode";
		}
		
		try {
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	if (type.getId()==null){
	    		session.saveOrUpdate(type);
	    	}else{
	    		session.merge(type);
	    	}
	    	trans.commit();
	    	initType();
	    	
			if(typeMaj == "act"){
				type.setContextActivite(true);
			}else if(typeMaj == "jur"){
				type.setContextJuridique(true);
			}else if(typeMaj == "marq"){
				type.setContextMarque(true);
			}else if(typeMaj == "mode"){
				type.setContextModele(true);
			}
	    	
	    	charger();
	    	FonctionsUtils.popup("Mise � jour effectu�e...");
	    	
		}catch(Exception e) {
			FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
			e.printStackTrace();			
		}
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public List<Type> getListtype() {
		return listtype;
	}

	public void setListtype(List<Type> listtype) {
		this.listtype = listtype;
	}

	public Boolean getContext() {
		return context;
	}

	public void setContext(Boolean context) {
		this.context = context;
	}

	
	
	
	//***************************************************************

	

}
