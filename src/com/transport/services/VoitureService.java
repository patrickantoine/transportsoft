package com.transport.services;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;

import com.app.util.FonctionsUtils;
import com.app.util.HibernateUtil;
import com.app.util.Login;
import com.app.util.PrintService;
import com.transport.entity.Licence;
import com.transport.entity.Voiture;

import net.sf.jasperreports.engine.JRException;


@Named
@SessionScoped
public class VoitureService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Inject
	private TransporteurService transporteurService;
	
	@Inject
	private Login login;
	
	@Inject
	private ProjetService projetService;
	
	@Inject
	private TypeService typeService;
	
	private Voiture voiture = null;
	private List<Voiture> listVoiture = null;
	private List<Licence> listLicence = null;
	private List<Licence> listLicence2 = null;
	private List<Licence> listLicence3 = null;
	private Licence licence = null;
	private int duree = 0;
	private Double montant1 = 0d;
	private Double montant2 = 0d;
	
	
	//***************************************************************
	
	@SuppressWarnings("unchecked")
	public void chargerLicencePerime() {
		try{
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	
	    	listLicence = session.createQuery("from Licence a where a.active = false and renouvelle = false and a.voiture.transporteur.active = true").list();
	    	trans.commit();
		}catch(Exception e){
    		e.printStackTrace();
    		FonctionsUtils.popupErreur("Erreur durant l'execution de la requ�te...");
    	}
	}
	
	@SuppressWarnings("unchecked")
	public void chargerLicencePerimePortefeuille() {
		try{
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	setListLicence2(session.createQuery("from Licence a where a.active = false and renouvelle = false and a.voiture.transporteur.active = true and a.voiture.transporteur.gestionnaire = :param").setParameter("param", login.getUserConnecte()).list());
	    	trans.commit();
		}catch(Exception e){
			e.printStackTrace();
			FonctionsUtils.popupErreur("Erreur durant l'execution de la requ�te...");
		}
	}
	
	@SuppressWarnings("unchecked")
	public void chargerLicenceExiprantBientotPortefeuille() {
		try{
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	
	    	setListLicence3(session.createQuery("from Licence l where l.active = true and DATEDIFF(l.dateFin,CURDATE()) < 30 and l.voiture.transporteur.gestionnaire = :param").setParameter("param", login.getUserConnecte()).list());
	    	trans.commit();
		}catch(Exception e){
			e.printStackTrace();
			FonctionsUtils.popupErreur("Erreur durant l'execution de la requ�te...");
		}
	}
	
	@SuppressWarnings("unchecked")
	public void chargerMontantLicenceActivePortefeuille() {
		
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	setMontant1((Double) session.createQuery("select sum(a.typeLicence.montant) from Licence a where a.active = true and a.voiture.transporteur.active = true and a.voiture.transporteur.gestionnaire = :param").setParameter("param", login.getUserConnecte()).uniqueResult());
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerMontantLicenceActiveGlobal() {
		
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	setMontant1((Double) session.createQuery("select sum(a.typeLicence.montant) from Licence a where a.active = true and a.voiture.transporteur.active = true").uniqueResult());
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerMontantLicenceExpireeGlobal() {
		
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	setMontant2((Double) session.createQuery("select sum(a.typeLicence.montant) from Licence a where a.active = false and renouvelle = false and a.voiture.transporteur.active = true ").uniqueResult());
    	trans.commit();
	}	
	
	@SuppressWarnings("unchecked")
	public void chargerMontantLicenceExpireePortefeuille() {
		try{
			Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			Transaction trans = session.beginTransaction();
			trans.begin();
    	
			setMontant2((Double) session.createQuery("select sum(a.typeLicence.montant) from Licence a where a.active = false and renouvelle = false and a.voiture.transporteur.active = true and a.voiture.transporteur.gestionnaire = :param").setParameter("param", login.getUserConnecte()).uniqueResult());
			trans.commit();
		}catch(Exception e){
			e.printStackTrace();
			FonctionsUtils.popupErreur("Erreur durant l'execution de la requ�te...");
		}
	}	
	
	@SuppressWarnings("unchecked")
	public void chargerLicenceActive() {
		try{
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	
	    	listLicence = session.createQuery("from Licence s where s.active = true and s.voiture.transporteur.active = true").list();
	    	trans.commit();
		}catch(Exception e){
			e.printStackTrace();
			FonctionsUtils.popupErreur("Erreur durant l'execution de la requ�te...");
		}
	}
	
	@SuppressWarnings("unchecked")
	public void chargerLicenceVoiture(Voiture vt) {
		

    	try{
        	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
        	Transaction trans = session.beginTransaction();
        	trans.begin();
        	
        	listLicence = session.createQuery("from Licence s where s.voiture = :param order by s.dateCreation desc").setParameter("param", vt).list();
        	trans.commit();
    	}catch(Exception e){
    		e.printStackTrace();
    		FonctionsUtils.popupErreur("Erreur durant l'execution de la requ�te...");
    	}
	}
	
	public void initLicence() {
		licence = new Licence();
		licence.setDateCreation(new Date());
		licence.setDateDebut(new Date());
		licence.setDateFin(FonctionsUtils.addAnnees(new Date(), 1));
		licence.setUserCreation(login.getUserConnecte());
	}
	
	
	@SuppressWarnings("unchecked")
	public void chargerVoiture() {
		
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listVoiture = session.createQuery("from Voiture s where s.transporteur.active = true order by s.transporteur.libelle").list();
    	trans.commit();
	}
	
	@SuppressWarnings("unchecked")
	public void chargerVoiturePortefeuille() {
		
    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	listVoiture = session.createQuery("from Voiture s where s.transporteur.active = true and s.transporteur.gestionnaire = :param order by s.transporteur.libelle").setParameter("param", login.getUserConnecte()).list();
    	trans.commit();
	}
	
	public void initVoiture() {
		voiture = new Voiture();
		voiture.setDateCreation(new Date());
		voiture.setUserCreation(login.getUserConnecte());
		initLicence();
	}
	
	public String versRenouvellement() {
		chargerLicencePerime();
		return "/private/licence/renouvellementLicence.xhtml?faces-redirect=true";
	}
	
	public String versFicheVehicule() {
		try {
			chargerLicenceVoiture(voiture);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return "/private/voiture/ficheVoiture.xhtml?faces-redirect=true";
	}
	
	public String versCreerLicence(Licence lic) {
		listLicence.clear();
		listLicence.add(lic);
		initLicence();
		return "/private/licence/addLicence.xhtml?faces-redirect=true";
	}
	
	public void chercherLicence() throws IOException{
		
		int param = 0;
		
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	Criteria criteria = session.createCriteria(Licence.class);
    	
    	criteria.createAlias("voiture", "voit",JoinType.LEFT_OUTER_JOIN);
    	criteria.createAlias("voit.transporteur", "trans",JoinType.LEFT_OUTER_JOIN);
    	
    	if(licence.getTypeLicence() != null && licence.getTypeLicence().getId() != null) {
    		criteria.add(Restrictions.eq("typeLicence", licence.getTypeLicence() ));param++;
    	}
    	
    	if(licence.getTypeActivite() != null && licence.getTypeActivite().getId() != null) {
    		criteria.add(Restrictions.eq("typeActivite", licence.getTypeActivite() ));param++;
    	}
    	
    	if(licence.getDateDebut() != null && licence.getDateFin()!=null) {
    		criteria.add(Restrictions.between("dateFin", licence.getDateDebut(), licence.getDateFin()));param++;
    	}
    	
    	if(transporteurService.getGestionnaire() != null && transporteurService.getGestionnaire().getId() != null) {
    		criteria.add(Restrictions.eq("trans.gestionnaire", transporteurService.getGestionnaire() ));param++;
    	}
    	
    	criteria.add(Restrictions.eq("trans.active", true ));
    	criteria.add(Restrictions.eq("active", licence.getActive() ));
    	
    	if(param > 0) {
        	listLicence = criteria.list();
        	
        	if(listLicence.isEmpty()) {
        		FonctionsUtils.popupWarning("Aucun r�sultat");
        	}else {
        		imprimerRechercheLicence();
        		FonctionsUtils.popup(listLicence.size()+" licence(s) trouv�e(s)");
        	}
    	}else {
    		FonctionsUtils.popupWarning("Veuillez renseigner au moins un param�tre...");
    	}
    	
    	trans.commit();

    	
	}
	
	@SuppressWarnings("unchecked")
	public void chercherVoiture() throws IOException {
		//SimpleDateFormat format  = new SimpleDateFormat("DD-MM-YYYY");
		//String ladate = format.format(journee).toString();
		
		int param = 0;
		
	    Session session = HibernateUtil.getSessionFactory().getCurrentSession();
    	Transaction trans = session.beginTransaction();
    	trans.begin();
    	
    	Criteria criteria = session.createCriteria(Voiture.class);
    	//criteria.add(Restrictions.sqlRestriction("TO_CHAR(dateCreationPaie,'DD-MM-YYYY') = '"+ladate+"'"));
    	

    	
    	criteria.createAlias("modele", "model",JoinType.LEFT_OUTER_JOIN);
    	criteria.createAlias("model.typeParent", "marque",JoinType.LEFT_OUTER_JOIN);
    	//criteria.createAlias("cot.personne", "personne",JoinType.LEFT_OUTER_JOIN);
    	criteria.createAlias("licence", "lic",JoinType.LEFT_OUTER_JOIN);
    	criteria.createAlias("lic.typeLicence", "typelic",JoinType.LEFT_OUTER_JOIN);
    	criteria.createAlias("lic.typeActivite", "typeact",JoinType.LEFT_OUTER_JOIN);
    	criteria.createAlias("transporteur", "trans",JoinType.LEFT_OUTER_JOIN);
    	
		/*
		 * if(voiture.getDateMiseCirculation() != null) {
		 * criteria.add(Restrictions.gt("dateMiseCirculation",FonctionsUtils.moinsNdays(
		 * voiture.getDateMiseCirculation(), 1) )); }
		 * 
		 * if(projetService.getDate1() != null) {
		 * criteria.add(Restrictions.lt("dateMiseCirculation",FonctionsUtils.addNdays(
		 * projetService.getDate1(), 1) )); }
		 */
    	
    	if(voiture.getModele()!= null && voiture.getModele().getId() != null) {
    		criteria.add(Restrictions.eq("modele", voiture.getModele()));param++;
    	}
    	if(voiture.getMotorisation() != null && voiture.getMotorisation().getId() != null) {
    		criteria.add(Restrictions.eq("motorisation", voiture.getMotorisation()));param++;
    	}
    	
    	if(voiture.getNumeroImmat().length() > 0) {
    		criteria.add(Restrictions.eq("numeroImmat", voiture.getNumeroImmat()));param++;
    	}
    	
    	if(voiture.getAnnee() > 0) {
    		criteria.add(Restrictions.eq("annee", voiture.getAnnee()));param++;
    	}
    	
    	if(voiture.getNumeroChassi().length() > 0) {
    		criteria.add(Restrictions.eq("numeroChassi", voiture.getNumeroChassi()));param++;
    	}
    	
    	if(voiture.getNombrePlace() > 0) {
    		criteria.add(Restrictions.eq("nombrePlace", voiture.getNombrePlace()));param++;
    	}
    	
    	if(projetService.getMarque() != null && projetService.getMarque().getId() != null) {
    		criteria.add(Restrictions.eq("model.typeParent", projetService.getMarque()));param++;
    	}
    	
    	if(transporteurService.getTransporteur() != null && transporteurService.getTransporteur().getId() != null) {
    		criteria.add(Restrictions.eq("transporteur", transporteurService.getTransporteur()));param++;
    	}
    	
    	if(projetService.getTypeLicence() != null && projetService.getTypeLicence().getId() != null) {
    		criteria.add(Restrictions.eq("lic.typeLicence", projetService.getTypeLicence() ));param++;
    	}
    	
    	if(projetService.getTypeActivite() != null && projetService.getTypeActivite().getId() != null) {
    		criteria.add(Restrictions.eq("lic.typeActivite", projetService.getTypeActivite() ));param++;
    	}
    	
    	criteria.add(Restrictions.eq("trans.active", true));
    	
    	//criteria.add(Restrictions.lt("dateCreationPaie", FonctionsUtils.addNdays(journee, 1)));
    	//criteria.add(Restrictions.gt("dateCreationPaie", FonctionsUtils.moinsNdays(journee, 1)));
    	
    	if(param > 0) {
        	listVoiture = criteria.list();
        	
        	if(listVoiture.isEmpty()) {
        		FonctionsUtils.popupWarning("Aucun r�sultat");
        	}else {
        		imprimeRecherche();
        		FonctionsUtils.popup(listVoiture.size()+" voiture(s) trouv�e(s)");
        	}
        	
    	}else {
    		FonctionsUtils.popupWarning("Veuillez renseigner au moins un param�tre...");
    	}
    	
    	trans.commit();

	}
	
	@SuppressWarnings("unchecked")
	public void imprimeRecherche() throws IOException {
		 Map parameters = new HashMap();
		 
		 if(projetService.getMarque() != null) {
			 parameters.put("MARQUE", projetService.getMarque().getLibelle());
		 }
		 if(voiture.getModele() != null) {
			 parameters.put("MODELE", voiture.getModele().getLibelle());
		 }
		 
		 if(voiture.getMotorisation() != null) {
			 parameters.put("MOTORISATION", voiture.getMotorisation().getLibelle());
		 }
		 
		 if(voiture.getTransporteur() != null) {
			 parameters.put("TRANSPORTEUR", voiture.getTransporteur().getLibelle());
		 }
		 
		 if(projetService.getTypeActivite() != null) {
			 parameters.put("ACTIVITE", projetService.getTypeActivite().getLibelle());
		 }
		 
		 if(projetService.getTypeLicence() != null) {
			 parameters.put("LICENCE", projetService.getTypeLicence().getLibelle());
		 }
		 
		 String deb = "";
		 String fin = "";
		 
		 if(voiture.getDateMiseCirculation() != null) {
			 deb = FonctionsUtils.formateDate(voiture.getDateMiseCirculation(), "dd/MM/yyyy");
		 }
		 
		 if(projetService.getDate1() != null) {
			 fin = FonctionsUtils.formateDate(projetService.getDate1(), "dd/MM/yyyy");
		 }
		 
		 List<Long> listID = new ArrayList<Long>();
		 
		 for(Voiture iv : listVoiture) {
			 listID.add(iv.getId());
		 }
		 
		 parameters.put("LISTID", listID);
		 parameters.put("CHASSIS", voiture.getNumeroChassi());
		 parameters.put("PLACES", voiture.getNombrePlace());
		 parameters.put("ANNEE", ""+voiture.getAnnee()+"");
		 parameters.put("MISEENCIRCULATION", deb+" - "+fin);
		 parameters.put("IMMATRICULATION", voiture.getNumeroImmat());
		 
		 
		 try {
			PrintService.genererEtat(null, "resultatVoiture", "resultatVoiture.pdf", parameters,"resultatVoiture.pdf");
		 } catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
	}
	
	@SuppressWarnings("unchecked")
	public void imprimerRechercheLicence() throws IOException {
		Map parameters = new HashMap();
		
		 if(licence.getTypeActivite() != null) {
			 parameters.put("ACTIVITE", licence.getTypeActivite().getLibelle());
		 }
		 
		 if(licence.getTypeLicence() != null) {
			 parameters.put("LICENCE", licence.getTypeLicence().getLibelle());
		 }
		 
		 if(licence.getDateDebut() != null && licence.getDateFin() != null) {
			 parameters.put("DEBUT", FonctionsUtils.formateDate(licence.getDateDebut(), "dd/MM/yyyy"));
			 parameters.put("FIN", FonctionsUtils.formateDate(licence.getDateFin(), "dd/MM/yyyy"));
		 }
		 
		 if(licence.getActive()) {
			 parameters.put("ACTIF", "Oui");
		 }else {
			 parameters.put("ACTIF", "Non"); 
		 }
		 
		 if(transporteurService.getGestionnaire() != null && transporteurService.getGestionnaire().getId() != null) {
			 parameters.put("GESTIONNAIRE", transporteurService.getGestionnaire().getPersonne().getNomComplet());
		 }
		 
		 List<Long> listID = new ArrayList<Long>();
		 
		 for(Licence iv : listLicence) {
			 listID.add(iv.getId());
		 }
		 parameters.put("LISTID", listID);
		 try {
			PrintService.genererEtat(null, "resultatLicence", "resultatLicence.pdf", parameters,"resultatLicence.pdf");
		 } catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		 }
	}
	
	public String versAddVoiture() {
		initVoiture();
		transporteurService.setTransporteur(null);
		transporteurService.chargerTransporteur();
		return "/private/voiture/addVoiture.xhtml?faces-redirect=true";
	}
	

	
	public String versListVoiture() {
		chargerVoiture();
		return "/private/voiture/listVoiture.xhtml?faces-redirect=true";
	}
	
	public String versRechercheVoiture() {
		transporteurService.chargerTransporteur();
		initVoiture();
		listVoiture = new ArrayList<Voiture>();
		return "/private/voiture/searchVoiture.xhtml?faces-redirect=true";
	}
	
	public String versRechercheLicence() {
		initLicence();
		licence.setDateDebut(null);
		licence.setDateFin(null);
		listLicence = new ArrayList<Licence>();
		return "/private/licence/searchLicence.xhtml?faces-redirect=true";
	}
	
	public String versLicencePerime() {
		chargerLicencePerime();
		return "/private/licence/listlicencePerime.xhtml?faces-redirect=true";
	}
	
	public String versLicenceActive() {
		chargerLicenceActive();
		return "/private/licence/listlicenceActive.xhtml?faces-redirect=true";
	}
	
	public void saveVoiture() {
		boolean ok = true;
		String msg = "";
		
		if(FonctionsUtils.date1AfterDate2(licence.getDateDebut(), licence.getDateFin())){
			ok=false;
			msg = "Veuillez v�rifier les dates de d�but et d'expiration de la licence...";
		}
		
		voiture.setTransporteur(transporteurService.getTransporteur());
		
		try {
			if(ok){
		    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		    	Transaction trans = session.beginTransaction();
		    	trans.begin();
		    	session.saveOrUpdate(voiture);
		    	
		    	licence.setVoiture(voiture);
		    	
		    	session.saveOrUpdate(licence);
		    	
		    	voiture.setLicence(licence);
		    	
		    	session.update(voiture);
		    	
		    	trans.commit();
		    	
		    	initVoiture();
		    	projetService.chargerInfo();
		    	FonctionsUtils.popup("Voiture mise � jour...");
			}else{
				FonctionsUtils.popupWarning(msg);
			}
			

		}catch(Exception e) {
			FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
			e.printStackTrace();			
		}
	}
	
	public void saveLicence() {
		
		boolean ok = true;
		String msg = "";
		
		if(FonctionsUtils.date1AfterDate2(licence.getDateDebut(), licence.getDateFin())){
			ok=false;
			msg = "Veuillez v�rifier les dates de d�but et d'expiration de la licence...";
		}
		
		licence.setVoiture(listLicence.get(0).getVoiture());
		licence.setRenouvellement(true);
		try {
				if(ok){
			    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
			    	Transaction trans = session.beginTransaction();
			    	trans.begin();
			    	
			    	session.saveOrUpdate(licence);
			    	
			    	listLicence.get(0).getVoiture().setLicence(licence);
			    	listLicence.get(0).setRenouvelle(true);
			    	session.merge(listLicence.get(0));
			    	session.merge(listLicence.get(0).getVoiture());
			    	
			    	trans.commit();
			    	projetService.chargerInfo();
			    	FonctionsUtils.popup("Licence renouvell�e...");
				}else{
					FonctionsUtils.popupWarning(msg);
				}
		}catch(Exception e) {
			FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
			e.printStackTrace();			
		}
	}
	
	
	
	//***************************************************************

	/**
	 * @return the voiture
	 */
	public Voiture getVoiture() {
		return voiture;
	}

	/**
	 * @param voiture the voiture to set
	 */
	public void setVoiture(Voiture voiture) {
		this.voiture = voiture;
	}

	/**
	 * @return the listVoiture
	 */
	public List<Voiture> getListVoiture() {
		return listVoiture;
	}

	/**
	 * @param listVoiture the listVoiture to set
	 */
	public void setListVoiture(List<Voiture> listVoiture) {
		this.listVoiture = listVoiture;
	}


	/**
	 * @return the licence
	 */
	public Licence getLicence() {
		return licence;
	}


	/**
	 * @param licence the licence to set
	 */
	public void setLicence(Licence licence) {
		this.licence = licence;
	}


	/**
	 * @return the listLicence
	 */
	public List<Licence> getListLicence() {
		return listLicence;
	}


	/**
	 * @param listLicence the listLicence to set
	 */
	public void setListLicence(List<Licence> listLicence) {
		this.listLicence = listLicence;
	}

	/**
	 * @return the duree
	 */
	public int getDuree() {
		return duree;
	}

	/**
	 * @param duree the duree to set
	 */
	public void setDuree(int duree) {
		this.duree = duree;
	}

	public List<Licence> getListLicence2() {
		return listLicence2;
	}

	public void setListLicence2(List<Licence> listLicence2) {
		this.listLicence2 = listLicence2;
	}

	public Double getMontant1() {
		return montant1;
	}

	public void setMontant1(Double montant1) {
		this.montant1 = montant1;
	}

	public Double getMontant2() {
		return montant2;
	}

	public void setMontant2(Double montant2) {
		this.montant2 = montant2;
	}

	public List<Licence> getListLicence3() {
		return listLicence3;
	}

	public void setListLicence3(List<Licence> listLicence3) {
		this.listLicence3 = listLicence3;
	}

}
