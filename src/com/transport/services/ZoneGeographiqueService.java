package com.transport.services;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.app.util.FonctionsUtils;
import com.app.util.HibernateUtil;
import com.transport.entity.Province;
import com.transport.entity.Type;
import com.transport.entity.Ville;
import com.transport.entity.Voiture;


@Named
@SessionScoped
public class ZoneGeographiqueService implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@Inject
	private ReferentielService referentielService;
	private Province province= null;
	private Ville ville= null;

	
	
	//***************************************************************

	public void initVille() {

		ville = new Ville();
		
	}
	
	public void initProvince() {

		province= new Province();
		
	}
	public String versAddProvince() {
		initProvince();
		
		return "/private/zoneGeographique/addProvince.xhtml?faces-redirect=true";
	}
	
	public String versAddVille() {
		initVille();
		return "/private/zoneGeographique/addVille.xhtml?faces-redirect=true";
	}
	
	
	public void saveProvince() {
		
		try {
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	if (province.getId()==null)
	    	session.saveOrUpdate(province);
	    	else 
	    		session.merge(province);
	    	trans.commit();
	    	initProvince();
	    	referentielService.chargerProvince();
	    	FonctionsUtils.popup(" mise � jour...");
	    	
		}catch(Exception e) {
			FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
			e.printStackTrace();			
		}
	}
	
	
public void saveVille() {
		
		try {
	    	Session session = HibernateUtil.getSessionFactory().getCurrentSession();
	    	Transaction trans = session.beginTransaction();
	    	trans.begin();
	    	if (ville.getId()==null)
	    	session.saveOrUpdate(ville);
	    	else 
	    		session.merge(ville);
	    	trans.commit();
	    	initVille();
	    	referentielService.chargerVille();
	    	FonctionsUtils.popup(" mise � jour...");
	    	
		}catch(Exception e) {
			FonctionsUtils.popupErreur("Une erreur est survenue durant cette action...");
			e.printStackTrace();			
		}
	}

	public Province getProvince() {
		return province;
	}

	public void setProvince(Province province) {
		this.province = province;
	}

	public Ville getVille() {
		return ville;
	}

	public void setVille(Ville ville) {
		this.ville = ville;
	}


	
	
	
	//***************************************************************

	

}
